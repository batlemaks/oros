CREATE TYPE USER_ROLE AS ENUM
(
    'student',
    'teacher',
    'assistant',
    'supervisor'
);

CREATE TYPE TASK_CATEGORY_TYPE AS ENUM
(
    'homework',
    'test'
);

CREATE TYPE TASK_TYPE AS ENUM
(
    'xlsx',
    'python',
    'test'
);

CREATE TYPE REQUEST_TYPE AS ENUM
(
    'add to group',
    'register user'
);

CREATE TYPE NOTIFICATION_TYPE AS ENUM
    (
        'notification',
        'news'
        );


CREATE TABLE IF NOT EXISTS user
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    token           TEXT        NOT NULL,
    name            TEXT        NOT NULL,
    role            USER_ROLE   NOT NULL,
    email           TEXT        NOT NULL,
    date            TIMESTAMP   DEFAULT now()
);


CREATE TABLE IF NOT EXISTS st_group
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    name            TEXT        NOT NULL,
    events          TEXT,
    date            TIMESTAMP   DEFAULT now()
);

CREATE TABLE IF NOT EXISTS deadline
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    task_id         UUID        NOT NULL,
    name            TEXT        NOT NULL,
    type            TASK_CATEGORY_TYPE,
    time            TIMESTAMP   NOT NULL,
    date            TIMESTAMP   DEFAULT now()
);

CREATE TABLE IF NOT EXISTS task_owner_link
(
    task_id      UUID        NOT NULL,
    owner_id     UUID        NOT NULL
);

CREATE TABLE IF NOT EXISTS group_student_link
(
    group_id        UUID        NOT NULL,
    student_id      UUID        NOT NULL
);

CREATE TABLE IF NOT EXISTS task_schema
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    name            TEXT        NOT NULL,
    description     TEXT,
    type            TASK_TYPE   NOT NULL,
    category        TASK_CATEGORY_TYPE NOT NULL,
    options         TEXT,
    active          BOOLEAN,
    date            TIMESTAMP   DEFAULT now()
);

CREATE TABLE IF NOT EXISTS task
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    schema_id       UUID,
    deadline_id     UUID,
    name            TEXT        NOT NULL,
    date            TIMESTAMP   DEFAULT now()
);

CREATE TABLE IF NOT EXISTS solved_task
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    task_id         UUID        NOT NULL,
    student_id      UUID        NOT NULL,
    student_name    TEXT        NOT NULL,
    name            TEXT        NOT NULL,
    inspector_id    UUID,
    mark            DOUBLE,
    description     TEXT,
    content         TEXT,
    date            TIMESTAMP   DEFAULT now()
);

CREATE TABLE IF NOT EXISTS material
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    name            TEXT        NOT NULL,
    description     TEXT,
    option          TEXT,
    active          BOOLEAN     DEFAULT FALSE,
    date            TIMESTAMP   DEFAULT now()
);

CREATE TABLE IF NOT EXISTS material_owner_link
(
    material_id     UUID        NOT NULL,
    owner_id        UUID        NOT NULL
);

CREATE TABLE IF NOT EXISTS material_file_link
(
    material_id     UUID        NOT NULL,
    file_id         UUID        NOT NULL
);

CREATE TABLE IF NOT EXISTS file
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    name            TEXT        NOT NULL,
    path            TEXT        NOT NULL,
    date            TIMESTAMP   DEFAULT now()
);

CREATE TABLE IF NOT EXISTS notification
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    author          TEXT        NOT NULL,
    msg             TEXT,
    tags            TEXT,
    type            NOTIFICATION_TYPE,
    is_viewed       BOOLEAN     DEFAULT TRUE,
    source          TEXT,
    date            TIMESTAMP   DEFAULT now()

/*    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    value           TEXT        NOT NULL,
    tags            TEXT,
    date            TIMESTAMP   DEFAULT now()*/
);

CREATE TABLE IF NOT EXISTS notification_owner_link
(
    notification_id UUID        NOT NULL,
    owner_id        UUID        NOT NULL,
    is_viewed       BOOLEAN     DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS request
(
    id              UUID        DEFAULT random_uuid() PRIMARY KEY,
    type            REQUEST_TYPE NOT NULL,
    user_id         UUID,
    source          TEXT,
    date            TIMESTAMP   DEFAULT now()
);

