


insert into user (id, token, name, role, email, date) values ('00000000-0000-0000-0000-000000000001', '001', 'Supervisor', 'supervisor', 'supervisor@hse.ru', '1973-07-13');

insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae000001', '002', 'Teacher name', 'teacher', 'teacher@hse.ru', '1973-07-13');

insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd111', 'a', 'Никитин Лев Владимирович', 'student', 'mail1@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd222', 'b', 'Поликарпов Николай Николаевич', 'student', 'mail2@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd333', 'c', 'Волков Дмитрий Максимович', 'student', 'mail3@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd444', 'd', 'Петров Илья Ильич', 'student', 'mail4@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd555', 'e', 'Гусев Максим Александрович', 'student', 'mail5@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd666', 'f', 'Федорова Кристина Никитична', 'student', 'mail6@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd777', 'g', 'Лебедева Вера Артёмовна', 'student', 'mail7@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd888', 'h', 'Михайлова Полина Марковна', 'student', 'mail8@hse.ru', '2021-12-02');
insert into user (id, token, name, role, email, date) values ('3f439272-6ae4-4cbb-b1cc-90b9ae5cd999', 'i', 'Беликова Сафия Матвеевна', 'student', 'mail9@hse.ru', '2021-12-02');

insert into st_group(id, name, events, date) values ('0f4995c3-ec84-4c2d-bf7a-1e3ec5d8998d', 'Группа 1', '{"lecture":{"date":"Понедельник","time":"1 Пара"},"seminar":{"date":"Понедельник","time":"2 Пара"}}', '2021-11-28');
insert into st_group(id, name, events, date) values ('78e971c9-5275-4de5-b480-a5c30acd2402', 'Группа 2', '{"lecture":{"date":"Понедельник","time":"1 Пара"},"seminar":{"date":"Понедельник","time":"5 Пара"}}', '2021-11-28');

insert into group_student_link(group_id, student_id) values ('0f4995c3-ec84-4c2d-bf7a-1e3ec5d8998d', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd111');
insert into group_student_link(group_id, student_id) values ('0f4995c3-ec84-4c2d-bf7a-1e3ec5d8998d', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd222');
insert into group_student_link(group_id, student_id) values ('0f4995c3-ec84-4c2d-bf7a-1e3ec5d8998d', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd333');
insert into group_student_link(group_id, student_id) values ('0f4995c3-ec84-4c2d-bf7a-1e3ec5d8998d', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd666');

insert into group_student_link(group_id, student_id) values ('78e971c9-5275-4de5-b480-a5c30acd2402', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd444');
insert into group_student_link(group_id, student_id) values ('78e971c9-5275-4de5-b480-a5c30acd2402', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd555');
insert into group_student_link(group_id, student_id) values ('78e971c9-5275-4de5-b480-a5c30acd2402', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd777');
insert into group_student_link(group_id, student_id) values ('78e971c9-5275-4de5-b480-a5c30acd2402', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd888');

insert into notification(id, author, msg, is_viewed, date) values ('65f16d99-b12e-4ec6-949c-e866f391e32c', 'Система', 'notification demo A1', true, '2021-09-28');
insert into notification(id, author, msg, is_viewed, date) values ('d5574a1c-be6b-41c8-b875-06d1a873fc17', 'Система', 'notification demo B1', true, '2021-09-28');
insert into notification(id, author, msg, is_viewed, date) values ('0fd1c883-87ba-494d-99fa-5e69c6567b00', 'Система', 'notification demo B2', true, '2021-09-28');

insert into notification_owner_link(notification_id, owner_id, is_viewed) values ('d5574a1c-be6b-41c8-b875-06d1a873fc17', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd111', false);
insert into notification_owner_link(notification_id, owner_id, is_viewed) values ('0fd1c883-87ba-494d-99fa-5e69c6567b00', '0f4995c3-ec84-4c2d-bf7a-1e3ec5d8998d', false);
insert into notification_owner_link(notification_id, owner_id, is_viewed) values ('65f16d99-b12e-4ec6-949c-e866f391e32c', '78e971c9-5275-4de5-b480-a5c30acd2402', false);

insert into material(id, name, description, active, date) values ('6811e2f8-3d10-47b1-9258-a52cd84a400f', 'Material A', 'Описание материала.', true, '2020-10-10');

insert into task_schema(id, name, description, type, category, options, active, date) values ('55a7629d-33dd-4809-95d8-8e21399d3087', 'Домашняя работа 1', 'ДЗ1', 'TEST', 'TEST', '{"test":[{"question":"Вопрос 1","answer":"Ответ 1"},{"question":"Вопрос 2","answer":"Ответ 2"},{}]}', true, '2021-12-02');
insert into task(id, schema_id, deadline_id, name, date) values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4a5fd', '55a7629d-33dd-4809-95d8-8e21399d3087', 'b679fd7f-409b-49a0-8e8d-2c9fa6e00000', 'Домашняя работа 1', '2021-12-02');
insert into deadline(id, task_id, name, type, time, date) values ('b679fd7f-409b-49a0-8e8d-2c9fa6e00000', 'b679fd7f-409b-49a0-8e8d-2c9fa6e4a5fd', 'Домашняя работа 1', 'TEST', '2022-09-25 14:42:53', '2021-12-02');
insert into task_owner_link(task_id, owner_id) values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4a5fd', '78e971c9-5275-4de5-b480-a5c30acd2402');

insert into solved_task(id, task_id, student_id, student_name, name, inspector_id, mark, content, date )
values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4111d', 'b679fd7f-409b-49a0-8e8d-2c9fa6e4a5fd', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd888',
        'Михайлова Полина Марковна', 'Михайлова Полина Марковна', '00000000-0000-0000-0000-000000000001', 8, '{"0":"Ответ 1","1":"Ответ 3"}','2022-10-10');

insert into solved_task(id, task_id, student_id, student_name, name, inspector_id, mark, content, date )
values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4333d', 'b679fd7f-409b-49a0-8e8d-2c9fa6e4a5fd', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd555',
        'Гусев Максим Александрович', 'Гусев Максим Александрович', '00000000-0000-0000-0000-000000000001', 6, '{"0":"Ответ 1","1":"Ответ 3"}','2022-10-10');

insert into solved_task(id, task_id, student_id, student_name, name, content, date )values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4222d', 'b679fd7f-409b-49a0-8e8d-2c9fa6e4a5fd', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd777','Лебедева Вера Артёмовна', 'Лебедева Вера Артёмовна', '{"0":"Ответ 1","1":"Ответ 45"}','2022-10-10');


insert into task_schema(id, name, description, type, category, options, active, date) values ('55a7629d-33dd-4809-95d8-8e21399d3999', 'Домашняя работа 2', 'ДЗ2', 'TEST', 'TEST', '{"test":[{"question":"Вопрос 999","answer":"Ответ 999"},{"question":"Вопрос 888","answer":"Ответ 888"},{}]}', true, '2021-12-03');
insert into task(id, schema_id, deadline_id, name, date) values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4a5f9', '55a7629d-33dd-4809-95d8-8e21399d3999', 'b679fd7f-409b-49a0-8e8d-2c9fa6e00001', 'Домашняя работа 2', '2021-12-03');
insert into deadline(id, task_id, name, type, time, date) values ('b679fd7f-409b-49a0-8e8d-2c9fa6e00001', 'b679fd7f-409b-49a0-8e8d-2c9fa6e4a5f9', 'Домашняя работа 2', 'TEST', '2022-09-25 14:42:53', '2021-12-03');
insert into task_owner_link(task_id, owner_id) values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4a5f9', '0f4995c3-ec84-4c2d-bf7a-1e3ec5d8998d');
insert into task_owner_link(task_id, owner_id) values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4a5f9', '78e971c9-5275-4de5-b480-a5c30acd2402');

insert into solved_task(id, task_id, student_id, student_name, name, inspector_id, mark, content, date )
values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4833d', 'b679fd7f-409b-49a0-8e8d-2c9fa6e4a5f9', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd555',
        'Гусев Максим Александрович', 'Гусев Максим Александрович', '00000000-0000-0000-0000-000000000001', 4, '{"0":"Ответ 1","1":"Ответ 3"}','2022-10-10');

insert into solved_task(id, task_id, student_id, student_name, name, inspector_id, mark, content, date )
values ('b679fd7f-409b-49a0-8e8d-2c9fa6e4373d', 'b679fd7f-409b-49a0-8e8d-2c9fa6e4a5f9', '3f439272-6ae4-4cbb-b1cc-90b9ae5cd111',
        'Никитин Лев Владимирович', 'Никитин Лев Владимирович', '00000000-0000-0000-0000-000000000001', 10, '{"0":"Ответ 1","1":"Ответ 3"}','2022-10-10');

insert into request(id, type,user_id,source,date) values ('b679fd7f-409b-49a0-8e8d-2c9f76e41233', 'add to group','3f439272-6ae4-4cbb-b1cc-90b9ae5cd999', '{"studentId":"3f439272-6ae4-4cbb-b1cc-90b9ae5cd999","studentName":"Беликова Сафия Матвеевна","to":"Группа 2","groupId":"78e971c9-5275-4de5-b480-a5c30acd2402"}','2022-10-10');

insert into notification(id, author, msg, tags, type, date)
values ('b679fd7f-409b-49a0-8e5d-2c9fa6e4a933', 'Преподаватель',
        'Новая система уже доступна!', '["Экзамен","Домашняя работа"]','news', '2021-12-03');