package ru.hse.oros.dao.entity

import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.UserRequest

@Repository
interface UserRequestDao : EntityDao<UserRequest> {
    fun findByOrderByDateAsc(): List<UserRequest>
}