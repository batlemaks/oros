package ru.hse.oros.dao.entity

import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.Deadline
import java.util.*

@Repository
interface DeadlineDao : EntityDao<Deadline> {
    fun findDistinctByOrderByName(): List<Deadline>
    fun findDistinctByOrderByDateAsc(): List<Deadline>

    @Query("SELECT DISTINCT d FROM Deadline d " +
//            "INNER JOIN Task t ON d.task_id = t.id " +
            "INNER JOIN TaskOwnerLink tol ON tol.task_id = d.task_id " +
            "" +
            "WHERE tol.owner_id IN " +
            "(SELECT g.id FROM Group g " +
            "INNER JOIN GroupStudentLink gsl ON gsl.group_id = g.id " +
            "WHERE gsl.student_id = :id) " +
            "" +
            "OR tol.owner_id = :id " +
            "ORDER BY d.date ASC")
    fun findStudentDeadlines(id: UUID): List<Deadline>

    @Query("SELECT COUNT(DISTINCT d) FROM Deadline d " +
//            "INNER JOIN Task t ON d.task_id = t.id " +
            "INNER JOIN TaskOwnerLink tol ON tol.task_id = d.task_id " +
            "" +
            "WHERE tol.owner_id IN " +
            "(SELECT g.id FROM Group g " +
            "INNER JOIN GroupStudentLink gsl ON gsl.group_id = g.id " +
            "WHERE gsl.student_id = :id) " +
            "" +
            "OR tol.owner_id = :id " +
            "ORDER BY d.date ASC")
    fun countStudentDeadlines(id: UUID): Int
}