package ru.hse.oros.dao.link

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.hse.oros.model.entity.link.GroupStudentLink
import ru.hse.oros.model.entity.link.GroupStudentLinkId
import java.util.*

@Repository
interface GroupStudentLinkDao : LinkDao<GroupStudentLink, GroupStudentLinkId> {
    @Transactional
    @Modifying
    @Query("DELETE FROM GroupStudentLink l WHERE l.group_id = :entity1Id AND l.student_id = :entity2Id")
    override fun delete(entity1Id: UUID, entity2Id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM GroupStudentLink l WHERE l.group_id = :entity1Id AND l.student_id IN :links")
    override fun delete(entity1Id: UUID, @Param("links") links: List<UUID>)

    @Transactional
    @Modifying
    @Query("DELETE FROM GroupStudentLink l WHERE l.group_id = :id")
    override fun deleteEntity1(id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM GroupStudentLink l WHERE l.student_id = :id")
    override fun deleteEntity2(id: UUID)


    @Query("SELECT l FROM GroupStudentLink l WHERE l.group_id = :entity1Id AND l.student_id IN :entity2Ids")
    fun findLinks(entity1Id: UUID, entity2Ids: List<UUID>): List<GroupStudentLink>


    @Query("SELECT DISTINCT l.student_id FROM GroupStudentLink l WHERE l.group_id = :groupId")
    fun findStudentsByGroupId(groupId: UUID): List<UUID>

}