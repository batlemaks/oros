package ru.hse.oros.dao.link

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.hse.oros.model.entity.link.NotificationOwnerLink
import ru.hse.oros.model.entity.link.NotificationOwnerLinkId
import java.util.*

@Repository
interface NotificationOwnerLinkDao : LinkDao<NotificationOwnerLink, NotificationOwnerLinkId> {
    @Transactional
    @Modifying
    @Query("DELETE FROM NotificationOwnerLink l WHERE l.notification_id = :entity1Id AND l.owner_id = :entity2Id")
    override fun delete(entity1Id: UUID, entity2Id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM NotificationOwnerLink l WHERE l.notification_id = :entity1Id AND l.owner_id IN :links")
    override fun delete(entity1Id: UUID, @Param("links") links: List<UUID>)

    @Transactional
    @Modifying
    @Query("DELETE FROM NotificationOwnerLink l WHERE l.notification_id = :id")
    override fun deleteEntity1(id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM NotificationOwnerLink l WHERE l.owner_id = :id")
    override fun deleteEntity2(id: UUID)


    @Query("SELECT DISTINCT l.notification_id FROM NotificationOwnerLink l WHERE l.owner_id = :ownerId")
    fun findNotificationIdsByOwner(ownerId: UUID): List<UUID>

    @Query("SELECT DISTINCT l.owner_id FROM NotificationOwnerLink l WHERE l.notification_id = :notificationId")
    fun findOwnersByNotificationId(notificationId: UUID): List<UUID>


    @Transactional
    @Modifying
    @Query("DELETE FROM NotificationOwnerLink nol " +
            "WHERE nol.owner_id IN " +
            "(SELECT gsl.student_id FROM GroupStudentLink gsl WHERE gsl.group_id = :groupId)" +
            "AND nol.notification_id IN " +
            "(SELECT l.notification_id FROM NotificationOwnerLink l WHERE l.owner_id = :groupId)")
    fun deleteGroupStudents(groupId: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM NotificationOwnerLink nol " +
            "WHERE nol.owner_id = :studentId " +
            "AND nol.notification_id IN " +
            "(SELECT l.notification_id FROM NotificationOwnerLink l WHERE l.owner_id = :groupId)")
    fun deleteGroupStudents(groupId: UUID, studentId: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM NotificationOwnerLink nol " +
            "WHERE nol.owner_id IN :ids " +
            "AND nol.notification_id IN " +
            "(SELECT l.notification_id FROM NotificationOwnerLink l WHERE l.owner_id = :groupId)")
    fun deleteGroupStudents(groupId: UUID, @Param("ids") studentIds: List<UUID>)


    @Transactional
    @Modifying
    @Query("UPDATE NotificationOwnerLink l SET l.is_viewed = :isViewed " +
            "WHERE l.notification_id = :notificationId AND l.owner_id = :ownerId")
    fun updateIsViewed(notificationId: UUID, ownerId: UUID,  isViewed: Boolean)

    @Transactional
    @Modifying
    @Query("UPDATE NotificationOwnerLink l SET l.is_viewed = :isViewed " +
            "WHERE l.notification_id IN :nIds AND l.owner_id = :ownerId")
    fun updateIsViewed(@Param("nIds") notificationIds: List<UUID>, ownerId: UUID, isViewed: Boolean)
}