package ru.hse.oros.dao.entity

import org.springframework.stereotype.Repository
import ru.hse.oros.model.entity.TaskSchema

@Repository
interface TaskSchemaDao : EntityDao<TaskSchema> {
    fun findByOrderByName(): List<TaskSchema>
    fun findByOrderByDateAsc(): List<TaskSchema>
}