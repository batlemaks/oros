package ru.hse.oros.dao.entity

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean
import ru.hse.oros.model.entity.AbstractEntity
import java.util.*

@NoRepositoryBean
interface EntityDao<T : AbstractEntity> : JpaRepository<T, UUID> {
//    fun findDistinctByOrderByName(): List<T>
}
