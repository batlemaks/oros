package ru.hse.oros.dao.link

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.hse.oros.model.entity.link.TaskOwnerLink
import ru.hse.oros.model.entity.link.TaskOwnerLinkId
import java.util.*

@Repository
interface TaskOwnerLinkDao : LinkDao<TaskOwnerLink, TaskOwnerLinkId> {
    @Transactional
    @Modifying
    @Query("DELETE FROM TaskOwnerLink l WHERE l.task_id = :entity1Id AND l.owner_id = :entity2Id")
    override fun delete(entity1Id: UUID, entity2Id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM TaskOwnerLink l WHERE l.task_id = :entity1Id AND l.owner_id IN :links")
    override fun delete(entity1Id: UUID, @Param("links") links: List<UUID>)

    @Transactional
    @Modifying
    @Query("DELETE FROM TaskOwnerLink l WHERE l.task_id = :id")
    override fun deleteEntity1(id: UUID)

    @Transactional
    @Modifying
    @Query("DELETE FROM TaskOwnerLink l WHERE l.owner_id = :id")
    override fun deleteEntity2(id: UUID)


    @Query("SELECT DISTINCT l.owner_id FROM TaskOwnerLink l WHERE l.task_id = :taskId")
    fun findOwnersByTaskId(taskId: UUID): List<UUID>

}