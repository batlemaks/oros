package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.model.entity.TaskSchema
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.TaskSchemaSaveRequest
import ru.hse.oros.model.request.entity.TaskSchemaUpdateRequest
import ru.hse.oros.service.entity.TaskSchemaService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/taskschemas", produces = [MediaType.APPLICATION_JSON_VALUE])
class TaskSchemaController(
        @Autowired override val service: TaskSchemaService
                          ) : BaseEntityController<TaskSchema>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping
    fun findAll(@CookieValue token: String,
                httpRequest: HttpServletRequest
               ): List<Any> {
        return super.findAll(token, httpRequest, AccessType.ALL)
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        return super.findById(token, httpRequest, AccessType.ALL, id)
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: TaskSchemaSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.OROS, request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: TaskSchemaUpdateRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.OROS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(@CookieValue token: String,
               @PathVariable id: UUID,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }
}


