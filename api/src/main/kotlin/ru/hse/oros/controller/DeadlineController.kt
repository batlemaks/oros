package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.model.entity.Deadline
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.DeadlineSaveRequest
import ru.hse.oros.model.request.entity.DeadlineUpdateRequest
import ru.hse.oros.service.entity.DeadlineService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/deadlines", produces = [MediaType.APPLICATION_JSON_VALUE])
class DeadlineController(
        @Autowired override val service: DeadlineService
                        ) : BaseEntityController<Deadline>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping("/count")
    fun count(@CookieValue token: String,
              httpRequest: HttpServletRequest
             ): Int {
        security.run(httpRequest, token, AccessType.ALL)
        return service.count()
    }

    @GetMapping
    fun findAll(@CookieValue token: String,
                httpRequest: HttpServletRequest
               ): List<Any> {
        return super.findAll(token, httpRequest, AccessType.ALL)
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        return super.findById(token, httpRequest, AccessType.ALL, id)
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: DeadlineSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.OROS, request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: DeadlineUpdateRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.OROS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(@CookieValue token: String,
               @PathVariable id: UUID,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }
}


