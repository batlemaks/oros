package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Material
import ru.hse.oros.model.request.entity.FileMaterialSaveRequest
import ru.hse.oros.model.request.entity.MaterialSaveRequest
import ru.hse.oros.service.entity.FileService
import ru.hse.oros.service.entity.MaterialService
import ru.hse.oros.service.link.MaterialFileLinkService
import ru.hse.oros.service.link.MaterialOwnerLinkService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/materials", produces = [MediaType.APPLICATION_JSON_VALUE])
class MaterialController(
    @Autowired override val service: MaterialService,
    @Autowired @Lazy private val fileService: FileService,
    @Autowired private val materialOwnerLinkService: MaterialOwnerLinkService,
    @Autowired private val materialFileLinkService: MaterialFileLinkService
                        ) : BaseEntityController<Material>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping
    fun findAll(@CookieValue token: String,
                httpRequest: HttpServletRequest
               ): List<Any> {
        return super.findAll(token, httpRequest, AccessType.ALL)
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        return super.findById(token, httpRequest, AccessType.ALL, id)
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: MaterialSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.OROS, request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: MaterialSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.OROS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(@CookieValue token: String,
               @PathVariable id: UUID,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }

    @PostMapping("/{id}/files/upload", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun uploadFile(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @RequestParam("file") file: MultipartFile,
        httpRequest: HttpServletRequest
                            ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)
/*        for (file in files) {
            if (!file.isEmpty) {
                map.add("images", MultipartInputStreamFileResource(file.inputStream, file.originalFilename))
            }
        }*/
        val fileId = fileService.create(FileMaterialSaveRequest(name = file.originalFilename?:let{"file"}, material_id = id), file).id
        return ResponseEntity(mapOf("id" to fileId), HttpStatus.CREATED)
    }

    @GetMapping("/files/{id}/download", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun downloadFile(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
                  ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)
        return ResponseEntity.ok(fileService.loadById(id))
    }

    @PostMapping("/{id}/students")
    fun createMaterialOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        materialOwnerLinkService.create(id, request)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PutMapping("/{id}/students")
    fun setMaterialOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        materialOwnerLinkService.set(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    @DeleteMapping("/{id}/students")
    fun deleteMaterialOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                                    ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        materialOwnerLinkService.delete(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    @GetMapping("/{id}/files")
    fun getMaterialFileLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
                               ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        return ResponseEntity.ok(service.getFilesToResponse(id))
    }


/*    @PostMapping("/{id}/files")
    fun createMaterialFileLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                               ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        materialFileLinkService.create(id, request)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PutMapping("/{id}/files")
    fun setMaterialFileLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                            ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        materialFileLinkService.set(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    @DeleteMapping("/{id}/files")
    fun deleteMaterialFileLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                               ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        materialFileLinkService.delete(id, request)
        return ResponseEntity(HttpStatus.OK)
    }*/

}


