package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import ru.hse.oros.enums.AccessType
import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.security.SecurityService
import ru.hse.oros.service.entity.EntityService
import java.util.*
import javax.servlet.http.HttpServletRequest


abstract class BaseEntityController<T: AbstractEntity> : EntityCrudController<T> {
    @Autowired protected lateinit var security: SecurityService


    protected abstract val logger: Logger
    protected abstract val service: EntityService<T>


    override fun findAll(token: String, httpRequest: HttpServletRequest, accessType: AccessType): List<Any> {
        security.run(httpRequest, token, accessType)

        return service.findAll()
    }

    override fun findById(token: String, httpRequest: HttpServletRequest, accessType: AccessType, id: UUID): ResponseEntity<Any> {
        security.run(httpRequest, token, accessType)

        return ResponseEntity.ok(service.findById(id))
    }

    override fun create(token: String, httpRequest: HttpServletRequest, accessType: AccessType, request: EntityRequest): ResponseEntity<Any> {
        security.run(httpRequest, token, accessType)

        val id = service.create(request).id
        return ResponseEntity(mapOf("id" to id), HttpStatus.CREATED)
    }

    override fun update(
            token: String,
            httpRequest: HttpServletRequest,
            accessType: AccessType,
            id: UUID,
            request: EntityRequest
                       ): ResponseEntity<Any> {
        security.run(httpRequest, token, accessType)

        service.update(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    override fun delete(token: String, httpRequest: HttpServletRequest, accessType: AccessType, id: UUID): ResponseEntity<Any> {
        security.run(httpRequest, token, accessType)

        service.delete(id)
        return ResponseEntity(HttpStatus.OK)
    }
}