package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.File
import ru.hse.oros.model.request.entity.FileMaterialSaveRequest
import ru.hse.oros.model.request.entity.FileSaveRequest
import ru.hse.oros.service.entity.FileService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/files", produces = [MediaType.APPLICATION_JSON_VALUE])
class FileController(
        @Autowired override val service: FileService
                    ) : BaseEntityController<File>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        return ResponseEntity.ok(service.findById(id))
    }

    @GetMapping("/{id}/download", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun loadById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        return ResponseEntity.ok(service.loadById(id))
    }

    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun create(@CookieValue token: String,
               @RequestParam file: MultipartFile,
               @Valid @RequestBody request: FileMaterialSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        val id = service.create(request, file).id
        return ResponseEntity(mapOf("id" to id), HttpStatus.CREATED)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: FileSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.ALL, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.ALL, id)
    }


    @PostMapping("/split", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE], produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun splitVariants(@CookieValue token: String,
               @RequestParam file: MultipartFile,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        return ResponseEntity.ok(service.splitVariants(file))
    }

}


