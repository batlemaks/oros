package ru.hse.oros.controller

import ru.hse.oros.logger.logger
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("", produces = [MediaType.APPLICATION_JSON_VALUE])
class StartController {

    val logger = logger(this)

    //    @PreAuthorize("hasRole('supervisor')")
    /* or hasRole('')*/
    @GetMapping("", "/"/*, produces = [MediaType.APPLICATION_JSON_VALUE]*/)
    @ResponseBody
    fun helloWorld(): ResponseEntity<Any?> {
        return ResponseEntity("Hello World OROS", HttpStatus.OK)
    }

    // TODO: login mapping

    /*    @DeleteMapping
fun delete(@RequestParam("id") id: UUID): String {*/


    /*
    @RequestParam params: Map<String, String>
    UUID.fromString(params["groupId"]), UUID.fromString(params["studentId"])*/

}
