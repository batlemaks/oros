package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.model.entity.Notification
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.NotificationSaveRequest
import ru.hse.oros.service.entity.NotificationService
import ru.hse.oros.service.link.NotificationOwnerLinkService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/notifications", produces = [MediaType.APPLICATION_JSON_VALUE])
class NotificationController(
        @Autowired override val service: NotificationService,
        @Autowired val notificationOwnerLinkService: NotificationOwnerLinkService
                            ) : BaseEntityController<Notification>() {

    override val logger: Logger
        get() = logger(this)

    @GetMapping("/count")
    fun count(@CookieValue token: String,
              httpRequest: HttpServletRequest
             ): Int {
        security.run(httpRequest, token, AccessType.STUDENTS)
        return service.count()
    }

    @GetMapping
    fun findAll(@CookieValue token: String,
               httpRequest: HttpServletRequest
                ): List<Any> {
        return super.findAll(token, httpRequest, AccessType.ALL)
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        return super.findById(token, httpRequest, AccessType.ALL, id)
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: NotificationSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.OROS, request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: NotificationSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.OROS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(
        @CookieValue token: String,
        @PathVariable id: UUID,
        httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.OROS, id)
    }

    @PostMapping("/{id}/students")
    fun createNotificationOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                            ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        notificationOwnerLinkService.create(id, request)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PutMapping("/{id}/students")
    fun setNotificationOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                             ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        notificationOwnerLinkService.set(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    @DeleteMapping("/{id}/students")
    fun deleteNotificationOwnerLinks(
        @CookieValue token: String,
        @PathVariable id: UUID,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                            ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        notificationOwnerLinkService.delete(id, request)
        return ResponseEntity(HttpStatus.OK)
    }

    @PutMapping("/viewed")
    fun setNotificationOwnerLinksView(
        @CookieValue token: String,
        @Valid @RequestBody request: List<UUID>, // students and groups
        httpRequest: HttpServletRequest
                                 ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.ALL)

        notificationOwnerLinkService.setIfViewed(request)
        return ResponseEntity(HttpStatus.OK)
    }

}


