package ru.hse.oros.controller

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import ru.hse.oros.model.entity.SolvedTask
import ru.hse.oros.enums.AccessType
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.TaskMarkRequest
import ru.hse.oros.model.request.entity.SolvedTaskSaveRequest
import ru.hse.oros.model.request.entity.SolvedTaskUpdateRequest
import ru.hse.oros.service.entity.SolvedTaskService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("/solvedtasks", produces = [MediaType.APPLICATION_JSON_VALUE])
class SolvedTaskController(
        @Autowired override val service: SolvedTaskService
                          ) : BaseEntityController<SolvedTask>() {

    override val logger: Logger
        get() = logger(this)


    @GetMapping
    fun findAll(@CookieValue token: String,
                httpRequest: HttpServletRequest
               ): List<Any> {
        return super.findAll(token, httpRequest, AccessType.OROS)
    }

    @GetMapping("/{id}")
    fun findById(@CookieValue token: String,
                 @PathVariable id: UUID,
                 httpRequest: HttpServletRequest
                ): ResponseEntity<Any> {
        return super.findById(token, httpRequest, AccessType.ALL, id)
    }

    @PostMapping("/{id}/mark")
    fun setMark(@CookieValue token: String,
                @PathVariable id: UUID,
                @Valid @RequestBody request: TaskMarkRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        security.run(httpRequest, token, AccessType.OROS)

        service.setMark(id, request)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PostMapping
    fun create(@CookieValue token: String,
               @Valid @RequestBody request: SolvedTaskSaveRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.create(token, httpRequest, AccessType.STUDENTS, request)
    }

    @PutMapping("/{id}")
    fun update(@CookieValue token: String,
               @PathVariable id: UUID,
               @Valid @RequestBody request: SolvedTaskUpdateRequest,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.update(token, httpRequest, AccessType.STUDENTS, id, request)
    }

    @DeleteMapping("/{id}")
    fun delete(@CookieValue token: String,
               @PathVariable id: UUID,
               httpRequest: HttpServletRequest
              ): ResponseEntity<Any> {
        return super.delete(token, httpRequest, AccessType.ALL, id)
    }
}


