package ru.hse.oros.enums

enum class UserRole(value: String) {
    STUDENT("student"),
    TEACHER("teacher"),
    ASSISTANT("assistant"),
    SUPERVISOR("supervisor")
}