package ru.hse.oros.enums

enum class TaskType(value: String) {
    XLSX("xlsx"),
    PYTHON("python"),
    TEST("test")
}