package ru.hse.oros.enums

enum class NotificationType(value: String) {
    NOTIFICATION("notification"),
    NEWS("news")
}