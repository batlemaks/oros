package ru.hse.oros.enums

enum class TaskCategoryType(value: String) {
    HOMEWORK("homework"),
    TEST("test")
}