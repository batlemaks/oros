package ru.hse.oros.enums

enum class AccessType {
    SUPERVISOR,
    TEACHERS,
    ASSISTANTS,
    STUDENTS,
    ALL,
    OROS    // all except students
}