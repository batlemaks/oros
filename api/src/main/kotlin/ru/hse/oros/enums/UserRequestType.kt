package ru.hse.oros.enums

enum class UserRequestType(value: String) {
    ADD_TO_GROUP("add to group"),
    REGISTER_USER("register user")
}