package ru.hse.oros.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.*

@ResponseStatus(HttpStatus.UNAUTHORIZED)
class InvalidTokenException(token: String? = null):
    RuntimeException("Invalid token${token?.let{"=$token"}?:""}")
