package ru.hse.oros.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.FORBIDDEN)
class AccessDeniedException(person: String? = null):
    RuntimeException("Access denied" + (person?.let{" for $person"}?:""))
