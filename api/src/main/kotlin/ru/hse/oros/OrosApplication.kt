package ru.hse.oros

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import ru.hse.oros.logger.logger


@SpringBootApplication
class OrosApplication

    fun main(args: Array<String>) {
        runApplication<OrosApplication>(*args)

        announceAppStartMsg()
    }




private fun announceAppStartMsg() {
    logger(OrosApplication::class).info("APP STARTED")

    println("===============================")
    println("========= APP STARTED =========")
    println("===============================")
}

