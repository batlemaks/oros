package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.enums.TaskCategoryType
import ru.hse.oros.enums.TaskType
import java.util.*

data class TaskSchemaSaveRequest(

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("description")
    val description: String? = null,

    @JsonProperty("type")
    @get:NotNull
    val type: TaskType,

    @JsonProperty("category")
    @get:NotNull
    val category: TaskCategoryType,

    @JsonProperty("options")
    val options: String? = null



                                ) : EntityRequest