package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import org.springframework.format.annotation.DateTimeFormat
import ru.hse.oros.enums.TaskCategoryType
import java.util.*

data class DeadlineSaveRequest(
    @JsonProperty("taskId")
    @get:NotNull
    val task_id: UUID,

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("type")
    @get:NotNull
    val type: TaskCategoryType,

    @JsonProperty("time")
    @get:NotNull
    val time: Date

                              ) : EntityRequest