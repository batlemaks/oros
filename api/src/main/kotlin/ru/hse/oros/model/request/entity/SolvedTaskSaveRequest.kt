package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class SolvedTaskSaveRequest(

    @JsonProperty("taskId")
    @get:NotNull
    val task_id: UUID,

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("inspectorId")
    val inspector_id: UUID? = null,

    @JsonProperty("mark")
    val mark: Double? = null,

    @JsonProperty("description")
    val description: String? = null,

    @JsonProperty("content")
    val content: String? = null

                                ) : EntityRequest