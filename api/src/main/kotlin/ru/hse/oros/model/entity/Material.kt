package ru.hse.oros.model.entity

import ru.hse.oros.enums.TaskCategoryType
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table
data class Material(
    @Id
    @GeneratedValue
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val name: String,
    val description: String? = null,
    val option: String? = null,
    val active: Boolean = false
                   ) : AbstractEntity()
