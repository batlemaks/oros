package ru.hse.oros.model.entity

import ru.hse.oros.enums.NotificationType
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table
data class Notification(
        @Id
        @GeneratedValue
        override val id: UUID = UUID.randomUUID(),
        override val date: Date = Date(),

        val author: String,
        val msg: String? = null,
        val tags: String? = null,
        val type: NotificationType? = NotificationType.NOTIFICATION,
        val is_viewed: Boolean = true,
        val source: String? = null
                       ) : AbstractEntity()

