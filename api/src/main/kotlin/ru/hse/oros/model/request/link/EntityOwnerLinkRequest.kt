package ru.hse.oros.model.request.link

import java.util.*

interface EntityOwnerLinkRequest: LinkRequest {
    val entity_id: UUID
    val owner_id: UUID
}