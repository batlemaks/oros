package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.enums.TaskCategoryType
import java.util.*

data class DeadlineUpdateRequest(

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("time")
    @get:NotNull
    val time: Date

                                ) : EntityRequest