package ru.hse.oros.model.entity.link

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "notification_owner_link")
@IdClass(NotificationOwnerLinkId::class)
data class NotificationOwnerLink(
    @Id
    val notification_id: UUID,
    @Id
    val owner_id: UUID,

    val is_viewed: Boolean = false
                           ): Link


@Embeddable
data class NotificationOwnerLinkId(
    val notification_id: UUID,
    val owner_id: UUID
                        ): Serializable, LinkId
