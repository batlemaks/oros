package ru.hse.oros.model.request.link

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class GroupStudentLinkRequest(
    @JsonProperty("groupId")
    @get:NotNull
    val group_id: UUID,
    override val entity1_id: UUID = group_id,

    @JsonProperty("studentId")
    @get:NotNull
    val student_id: UUID,
    override val entity2_id: UUID = student_id,

    ) : EntitiesLinkRequest