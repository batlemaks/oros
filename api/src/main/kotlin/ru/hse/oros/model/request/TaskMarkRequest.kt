package ru.hse.oros.model.request

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.enums.TaskCategoryType
import java.util.*

data class TaskMarkRequest(
    @JsonProperty("mark")
    @get:NotNull
    val mark: Double,

                      )