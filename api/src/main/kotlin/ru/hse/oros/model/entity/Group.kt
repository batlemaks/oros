package ru.hse.oros.model.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "st_group")
data class Group(
    @Id
    @GeneratedValue
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val name: String,
    val events: String? = null
                ) : AbstractEntity()
