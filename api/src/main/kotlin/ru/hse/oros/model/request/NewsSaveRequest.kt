package ru.hse.oros.model.request

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.model.request.entity.EntityRequest

data class NewsSaveRequest(
    @JsonProperty("value")
    @get:NotNull
    val value: String,

    @JsonProperty("tags")
    val tags: String? = null
                          ) : EntityRequest