package ru.hse.oros.model.response

import ru.hse.oros.enums.UserRole
import ru.hse.oros.model.entity.User
import java.util.*

data class UserResponse(
    val id: UUID = UUID.randomUUID(),
    val date: Date = Date(),

    val name: String,
    val role: UserRole,
    val email: String
                       ) {
    constructor(user: User):
            this(id = user.id, date = user.date, name = user.name, role = user.role, email = user.email)
}
