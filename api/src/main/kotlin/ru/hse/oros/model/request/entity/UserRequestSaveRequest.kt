package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.enums.UserRequestType
import java.util.*

data class UserRequestSaveRequest(
        @JsonProperty("userId")
        @get:NotNull
        val userId: UUID,

        @JsonProperty("type")
        @get:NotNull
        val type: UserRequestType,

        @JsonProperty("source")
        val source: String? = null
                                 ) : EntityRequest
