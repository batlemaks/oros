package ru.hse.oros.model.entity

import ru.hse.oros.enums.TaskCategoryType
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table
data class Deadline(
    @Id
    @GeneratedValue
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val task_id: UUID,
    val name: String,
    val type: TaskCategoryType,
    val time: Date
                   ) : AbstractEntity()
