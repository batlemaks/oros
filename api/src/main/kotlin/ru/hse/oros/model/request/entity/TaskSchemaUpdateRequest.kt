package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import ru.hse.oros.enums.TaskCategoryType
import ru.hse.oros.enums.TaskType
import java.util.*

data class TaskSchemaUpdateRequest(

    @JsonProperty("name")
    val name: String? = null,

    @JsonProperty("description")
    val description: String? = null,

    @JsonProperty("type")
    val type: TaskType? = null,

    @JsonProperty("category")
    val category: TaskCategoryType? = null,

    @JsonProperty("options")
    val options: String? = null

                                  ) : EntityRequest