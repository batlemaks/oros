package ru.hse.oros.model.entity.link

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "group_student_link")
@IdClass(GroupStudentLinkId::class)
data class GroupStudentLink(
    @Id
    val group_id: UUID,
    @Id
    val student_id: UUID
                           ): Link


@Embeddable
data class GroupStudentLinkId(
    val group_id: UUID,
    val student_id: UUID
                             ): Serializable, LinkId
