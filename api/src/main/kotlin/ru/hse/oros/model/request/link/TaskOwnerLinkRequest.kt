package ru.hse.oros.model.request.link

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class TaskOwnerLinkRequest(
    @JsonProperty("taskId")
    @get:NotNull
    override val entity_id: UUID,

    @JsonProperty("ownerId")
    @get:NotNull
    override val owner_id: UUID
                               ) : EntityOwnerLinkRequest