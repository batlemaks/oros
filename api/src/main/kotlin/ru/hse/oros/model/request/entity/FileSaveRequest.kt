package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull

data class FileSaveRequest(

    @JsonProperty("name")
    @get:NotNull
    val name: String

                          ) : EntityRequest