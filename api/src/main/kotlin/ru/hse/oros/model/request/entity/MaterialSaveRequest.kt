package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class MaterialSaveRequest(

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("description")
    val description: String? = null,

    @JsonProperty("option")
    val option: String? = null,

    @JsonProperty("active")
    val active: Boolean = false,

    @JsonProperty("links")
    val links: List<UUID>? = null

                              ) : EntityRequest