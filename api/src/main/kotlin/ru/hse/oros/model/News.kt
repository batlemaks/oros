package ru.hse.oros.model

import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.entity.Notification
import java.util.*


data class News(
    override val id: UUID = UUID.randomUUID(),
    override val date: Date = Date(),

    val value: String? = null,
    val tags: String? = null
               ): AbstractEntity() {

    constructor(notification: Notification) :
            this(
                id = notification.id,
                date = notification.date,
                value = notification.msg,
                tags = notification.tags,
                )
}
