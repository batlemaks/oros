package ru.hse.oros.model.request.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import java.util.*

data class FileMaterialSaveRequest(

    @JsonProperty("name")
    @get:NotNull
    val name: String,

    @JsonProperty("material_id")
    @get:NotNull
    val material_id: UUID

                                  ) : EntityRequest