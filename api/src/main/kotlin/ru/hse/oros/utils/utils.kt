package ru.hse.oros.utils

import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.entity.User
import ru.hse.oros.model.response.UserResponse
import java.text.SimpleDateFormat
import kotlin.reflect.full.memberProperties

val sdf = SimpleDateFormat("dd/mm/yyyy")

inline fun <reified T : AbstractEntity> T.asMap(): MutableMap<String, Any?> {
    val props = T::class.memberProperties.associateBy { it.name }
    return props.keys.associateWith { props[it]?.get(this) } as MutableMap<String, Any?>
}

fun List<User>.toUserResponse(): List<UserResponse> {
    return this.map { UserResponse(it) }
}

fun User.toUserResponse(): UserResponse {
    return UserResponse(this)
}

/*
fun JSONObject.toMap(): Map<String, Any?> = keys().asSequence().associateWith {
    when (val value = this[it])
    {
        is JSONArray ->
        {
            val map = (0 until value.length()).associate { Pair(it.toString(), value[it]) }
            JSONObject(map).toMap().values.toList()
        }
        is JSONObject -> value.toMap()
        JSONObject.NULL -> null
        else            -> value
    }
}*/
