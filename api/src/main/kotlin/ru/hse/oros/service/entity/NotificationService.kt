package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.Notification
import ru.hse.oros.model.request.entity.NotificationSaveRequest
import java.util.*


interface NotificationService: EntityService<Notification> {
    fun count(): Int
    fun delete(ids: List<UUID>)
    fun create(request: NotificationSaveRequest, owner: UUID): Notification
    fun create(request: NotificationSaveRequest, owners: List<UUID>): Notification
}