package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.core.io.FileSystemResource
import org.apache.commons.*
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import ru.hse.oros.dao.entity.FileDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.File
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.model.request.entity.FileMaterialSaveRequest
import ru.hse.oros.model.request.entity.FileSaveRequest
import ru.hse.oros.model.request.link.MaterialFileLinkRequest
import ru.hse.oros.service.ApachePoiService
import ru.hse.oros.service.link.MaterialFileLinkService
import ru.hse.oros.utils.asMap
import java.io.ByteArrayOutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import java.util.stream.Stream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import java.io.File as JFile


@Service
class FileServiceImpl(
        @Autowired override val dao: FileDao,
        @Autowired @Lazy private val materialFileLinkService: MaterialFileLinkService,
        @Autowired @Lazy private val materialService: MaterialService,
        @Autowired @Lazy private val apachePoiService: ApachePoiService
                     ) : AbstractEntityService<File>(), FileService {

    override val entityName: String
        get() = "File"

    final override val logger
        get() = logger(this)

    private val rootLocation: Path = Paths.get("fdir")
    private val tempLocation: Path = Paths.get("fdir/temp")

    init {
        try {
            Files.createDirectory(rootLocation)
            logger.info("FILE fdir")
        } catch (e: Exception) {
            logger.error("FILE fdir ERROR")
        }
        try {
            Files.createDirectory(tempLocation)
            logger.info("FILE temp")
        } catch (e: Exception) {
            logger.error("FILE temp ERROR")
        }
    }

    override fun daoFindAll(): List<File> {
        return dao.findByOrderByDateAsc()
    }

    override fun loadAll(materialId: UUID): List<Resource> {
        val response = materialService.getFiles(materialId).map { file ->
            loadFile(file)
        }

        return response
    }

    override fun loadFile(file: File): Resource {
        val tempPath = tempLocation.resolve(file.name)
        JFile(tempLocation.toString()).listFiles()!!.forEach { it.deleteRecursively() }

        Files.copy(rootLocation.resolve(file.path), tempPath)

        val resource = UrlResource(tempPath.toUri())

        if (resource.exists() || resource.isReadable) {
            return resource
        } else {
            throw RuntimeException("File loading fail -> name = ${file.name}, path = ${file.path}")
        }
    }

    override fun findById(id: UUID): Any {
        logger.info("Find $entityName with id = $id")
        val file = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        return file.asMap().apply { this.remove("path") }
    }

    override fun loadById(id: UUID): Any {
        logger.info("Find $entityName with id = $id")
        val file = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        return loadFile(file)
    }

    override fun create(request: FileMaterialSaveRequest, file: MultipartFile): File {
        logger.info("Create new $entityName with name=${request.name}")

        materialService.findById(request.material_id)

        val path = store(file)
        val response = dao.save(
            File(
                name = request.name,
                path = path
                )
                               )

        materialFileLinkService.create(
            MaterialFileLinkRequest(material_id = request.material_id, file_id = response.id)
                                      )

        return response
    }

    override fun create(request: EntityRequest): File {
        request as FileMaterialSaveRequest
        logger.info("Create new $entityName with name=${request.name}")

        val response = dao.save(
            File(
                name = request.name,
                path = randomName()
                )
                               )

        materialFileLinkService.create(
            MaterialFileLinkRequest(material_id = request.material_id, file_id = response.id)
                                      )

        return response
    }

    override fun newEntity(entity: File, request: EntityRequest): File {
        request as FileSaveRequest

        return entity.copy(
            name = request.name
                          )
    }

    override fun store(file: MultipartFile): String {
        val path = rootLocation.resolve(randomName())
        Files.copy(file.inputStream, path)
        return path.fileName.toString()
    }

    fun randomName(): String {
        val charset = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        return List((15..20).random()) { charset.random() }.joinToString("")
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val file = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        materialFileLinkService.deleteFile(id)
        Files.delete(rootLocation.resolve(file.path))
        dao.delete(file)
    }


    override fun splitVariants(file: MultipartFile): ByteArray {
        var fileName = file.originalFilename ?: "file"
        fileName = fileName.removeRange(fileName.lastIndexOf('.'), fileName.lastIndex + 1)

        println(file.originalFilename)
        println(fileName)

        val variants = apachePoiService.splitVariants(file.inputStream)

        val extension = ".xlsx"
        val baos = ByteArrayOutputStream()
        val zos = ZipOutputStream(baos)

        for ((index, variant) in variants.withIndex()) {
            val entry = ZipEntry(fileName + " ${index + 1}" + extension)
            entry.size = variant.size.toLong()
            zos.putNextEntry(entry)
            zos.write(variant)
        }

        zos.closeEntry()
        zos.close()
        return baos.toByteArray()
    }
}
