package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.DeadlineDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.Deadline
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.DeadlineSaveRequest
import ru.hse.oros.model.request.entity.DeadlineUpdateRequest
import ru.hse.oros.model.request.entity.EntityRequest
import java.util.*

@Service
class DeadlineServiceImpl(
    @Autowired override val dao: DeadlineDao,
    @Autowired @Lazy private val taskService: TaskService
        ) : AbstractEntityService<Deadline>(), DeadlineService {

    override val entityName: String
        get() = "Deadline"

    override val logger
        get() = logger(this)

    override fun count(): Int {
        logger.info("Count $entityName's")
        val client = security.getClient()

        return if (security.isOROS(client.role))
            dao.count().toInt()
        else
            dao.countStudentDeadlines(client.id)
    }

    override fun daoFindAll(): List<Deadline> {
        val client = security.getClient()

        return if (security.isOROS(client.role))
            dao.findDistinctByOrderByDateAsc()
        else
            dao.findStudentDeadlines(client.id)
    }

    override fun create(request: EntityRequest): Deadline {
        request as DeadlineSaveRequest
        logger.info("Create new $entityName with name=${request.name}")

        taskService.findById(request.task_id) // throw NotFound

        val deadline = dao.save(
            Deadline(
                task_id = request.task_id,
                name = request.name,
                type = request.type,
                time = request.time
                    )
                               )

        taskService.addDeadline(request.task_id, deadline.id)

        return deadline
    }

    override fun newEntity(entity: Deadline, request: EntityRequest): Deadline {
        request as DeadlineUpdateRequest

        return entity.copy(
            name = request.name,
            time = request.time
                          )
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val deadline = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        taskService.deleteDeadline(deadline.task_id, deadline.id)
        dao.delete(deadline)
    }
}