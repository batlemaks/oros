package ru.hse.oros.service.link

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import ru.hse.oros.dao.link.LinkDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.entity.link.Link
import ru.hse.oros.model.entity.link.LinkId
import ru.hse.oros.model.request.link.EntityOwnerLinkRequest
import ru.hse.oros.model.request.link.LinkRequest
import ru.hse.oros.security.SecurityService
import ru.hse.oros.service.entity.EntityService
import ru.hse.oros.service.entity.GroupService
import ru.hse.oros.service.entity.UserService
import java.util.*


abstract class AbstractEntityOwnerLinkService<T: Link, ID: LinkId, E: AbstractEntity, R: EntityOwnerLinkRequest>: EntityOwnerLinkService<T> {
    @Autowired
    protected lateinit var security: SecurityService
    @Autowired
    @Lazy
    protected lateinit var userService: UserService
    @Autowired
    @Lazy
    protected lateinit var groupService: GroupService

    protected abstract val logger: Logger
    protected abstract val dao: LinkDao<T, ID>
    protected abstract val entityService: EntityService<E>


    /*    override fun create(links: List<LinkRequest>) {
        links as List<TaskOwnerLinkSaveRequest>
        logger.info("Create new ${entityName}s: ")

    }*/

    override fun set(id: UUID, entityIds: List<UUID>) {
        logger.info("Set new $entityName with link list and entity id = $id")

        entityService.findById(id)
        val entityOwnerLinks = mutableListOf<T>()

        for (link in entityIds) {
            if (!existsOwner(link)) throw EntityNotFountException(link)
            entityOwnerLinks.add(newEntityOwnerLink(id, link))
        }

        dao.deleteEntity1(id)
        dao.saveAll(entityOwnerLinks)
    }

    override fun create(id: UUID, entityIds: List<UUID>) {
        logger.info("Create new $entityName with link list and entity id = $id")

        entityService.findById(id)
        val entityOwnerLinks = mutableListOf<T>()

        for (link in entityIds) {
            if (!existsOwner(link)) throw EntityNotFountException(link)
            entityOwnerLinks.add(newEntityOwnerLink(id, link))
        }

        dao.saveAll(entityOwnerLinks)
    }

    protected abstract fun newEntityOwnerLink(entityId: UUID, ownerId: UUID): T

    override fun existsOwner(id: UUID): Boolean {
        return (userService.findByIdOrNull(id) != null
                || groupService.findByIdOrNull(id) != null)
    }

    override fun getOwner(id: UUID): Any? {
        return userService.findByIdOrNull(id) ?: let { groupService.findByIdOrNull(id) }
    }

    override fun create(request: LinkRequest) {
        request as R
        logger.info("Create new $entityName with entity id = ${request.entity_id} & owner id = ${request.owner_id}")

        entityService.findById(request.entity_id)
        if (!existsOwner(request.owner_id)) throw EntityNotFountException(request.owner_id)

        dao.save(newEntityOwnerLink(request.entity_id, request.owner_id))
    }

    override fun delete(request: LinkRequest) {
        request as R
        logger.info("Delete $entityName with entity id = ${request.entity_id} & owner id = ${request.owner_id}")

        entityService.findById(request.entity_id)
        if (!existsOwner(request.owner_id)) throw EntityNotFountException(request.owner_id)

        dao.delete(request.entity_id, request.owner_id)
    }

    override fun delete(id: UUID, entityIds: List<UUID>) {
        logger.info("Delete ${entityName}s with link list and entity id = $id")

        entityService.findById(id)

        for (link in entityIds) {
            if (!existsOwner(link)) throw EntityNotFountException(link)
        }

        dao.delete(id, entityIds)
    }

    override fun deleteEntity(id: UUID) {
        logger.info("Delete $entityName with entity id = $id")

        entityService.findById(id)
        dao.deleteEntity1(id)
    }

    override fun deleteOwner(id: UUID) {
        logger.info("Delete $entityName with owner id = $id")

        if (!existsOwner(id)) throw EntityNotFountException(id)
        dao.deleteEntity2(id)
    }
}