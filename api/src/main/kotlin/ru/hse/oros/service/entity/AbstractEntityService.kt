package ru.hse.oros.service.entity

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import ru.hse.oros.dao.entity.EntityDao
import ru.hse.oros.exception.AccessDeniedException
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.security.SecurityService
import java.util.*


abstract class AbstractEntityService<T: AbstractEntity>: EntityService<T> {
    @Autowired
    protected lateinit var security: SecurityService

    protected abstract val logger: Logger
    protected abstract val dao: EntityDao<T>

    override fun findAll(): List<Any> {
        logger.info("Find all ${entityName}s")
        return daoFindAll()
    }

    protected abstract fun daoFindAll(): List<T>

    override fun findById(id: UUID): Any {
        logger.info("Find $entityName with id = $id")
        return dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
    }

    override fun findByIdOrNull(id: UUID): Any? {
        logger.info("Find $entityName with id = $id")
        return dao.findByIdOrNull(id)
    }

    override fun update(id: UUID, request: EntityRequest) {
        logger.info("Update $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        dao.save(newEntity(entity, request))
    }
    protected abstract fun newEntity(entity: T, request: EntityRequest): T

    protected open fun updateByOrosOrAuthor(id: UUID, request: EntityRequest) {
        logger.info("Update $entityName with id = $id")
        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        val clientId = security.getClient().id
        if (!security.isOROS(clientId) && clientId != id) throw AccessDeniedException()

        dao.save(newEntity(entity, request))
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        dao.delete(entity)
    }

    protected open fun deleteByOrosOrAuthor(id: UUID) {
        logger.info("Delete $entityName with id = $id")
        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        val clientId = security.getClient().id
        if (!security.isOROS(clientId) && clientId != id) throw AccessDeniedException()

        dao.delete(entity)
    }
}