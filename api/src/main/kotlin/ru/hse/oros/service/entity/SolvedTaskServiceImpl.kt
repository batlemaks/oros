package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.SolvedTaskDao
import ru.hse.oros.exception.AccessDeniedException
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.SolvedTask
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.TaskMarkRequest
import ru.hse.oros.model.request.entity.SolvedTaskSaveRequest
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.model.request.entity.NotificationSaveRequest
import ru.hse.oros.model.request.entity.SolvedTaskUpdateRequest
import java.util.*

@Service
class SolvedTaskServiceImpl(
        @Autowired override val dao: SolvedTaskDao,
        @Autowired @Lazy private val notificationService: NotificationService
                           ) : AbstractEntityService<SolvedTask>(), SolvedTaskService {

    override val entityName: String
        get() = "SolvedTask"

    override val logger
        get() = logger(this)


    override fun daoFindAll(): List<SolvedTask> {
        return dao.findDistinctByOrderByDateAsc()
    }

    override fun findById(id: UUID): Any {
        logger.info("Find $entityName with id = $id")

        val clientId = security.getClient().id
        if (!security.isOROS(clientId) && clientId != id) throw AccessDeniedException()

        return dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
    }

    override fun create(request: EntityRequest): SolvedTask {
        request as SolvedTaskSaveRequest
        logger.info("Create new $entityName with task id = ${request.task_id}")

        val client = security.getClient()

        return dao.save(
            SolvedTask(
                task_id = request.task_id,
                name = request.name,
                student_id = client.id,
                student_name = client.name,
                inspector_id = request.inspector_id,
                mark = request.mark,
                description = request.description,
                content = request.content
                      )
                       )
    }

    override fun update(id: UUID, request: EntityRequest) {
        return super.updateByOrosOrAuthor(id, request)
    }

    override fun newEntity(entity: SolvedTask, request: EntityRequest): SolvedTask {
        request as SolvedTaskUpdateRequest

        return entity.copy(
            description = request.description,
            content = request.content
                          )
    }

    override fun delete(id: UUID) {
        super.deleteByOrosOrAuthor(id)
    }

    override fun setMark(id: UUID, request: TaskMarkRequest) {
        logger.info("Set mark to the $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        val clientId = security.getClient().id

        notificationService.create(
            NotificationSaveRequest(author = "Система", msg = "Задание: ${entity.name} \n Оценка: ${request.mark}"),
            entity.student_id
                                  )

        dao.save(
            entity.copy(
                inspector_id = clientId,
                mark = request.mark
                       )
                )
    }

    override fun deleteStudentTasks(id: UUID) {
        logger.info("Delete student tasks with student id = $id")
        dao.deleteStudentTasks(id)
    }
}