package ru.hse.oros.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.NotificationDao
import ru.hse.oros.enums.NotificationType
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.logger.logger
import ru.hse.oros.model.News
import ru.hse.oros.model.entity.Notification
import ru.hse.oros.model.request.NewsSaveRequest
import ru.hse.oros.security.SecurityService
import java.util.*


@Service
class NewsServiceImpl(
    @Autowired @Lazy private val dao: NotificationDao,
    @Autowired @Lazy private val security: SecurityService
    ) : NewsService {

    override val entityName: String
    get() = "News"

    private val logger = logger(this)


    override fun findAll(): List<News> {
        logger.info("Find all $entityName")
        return daoFindAll()
    }

    fun daoFindAll(): List<News> {
        return dao.findNewsByOrderByDate().map { News(it) }
    }

    override fun findById(id: UUID): News {
        logger.info("Find $entityName with id = $id")
        val notification = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        return News(notification)
    }

    override fun findByIdOrNull(id: UUID): News? {
        logger.info("Find $entityName with id = $id")
        val notification = dao.findByIdOrNull(id)
        return notification?.let { News(notification) }
    }


    override fun create(request: NewsSaveRequest): News {
        logger.info("Create new $entityName with value=${request.value}")
        val client = security.getClient()
        val notification = dao.save(
            Notification(
                msg = request.value,
                tags = request.tags,
                author = client.name,
                type = NotificationType.NEWS
                        )
                                   )
        return News(notification)
    }

    override fun update(id: UUID, request: NewsSaveRequest) {
        logger.info("Update $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        dao.save(newEntity(entity, request))
    }

    fun newEntity(entity: Notification, request: NewsSaveRequest): Notification {
        return entity.copy(
            msg = request.value,
            tags = request.tags
                          )
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        dao.delete(entity)
    }
}