package ru.hse.oros.service

import java.io.InputStream


interface ApachePoiService {
    val entityName: String

    fun createGradebookReport(gradebook: Map<String, Any?>): ByteArray
    fun splitVariants(stream: InputStream): List<ByteArray>
}