package ru.hse.oros.service.entity

 import org.springframework.beans.factory.annotation.Autowired
 import org.springframework.context.annotation.Lazy
 import org.springframework.data.repository.findByIdOrNull
 import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.UserDao
 import ru.hse.oros.enums.UserRole
 import ru.hse.oros.exception.AccessDeniedException
 import ru.hse.oros.model.entity.User
import ru.hse.oros.exception.BadRequestException
 import ru.hse.oros.exception.EntityNotFountException
 import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.model.request.entity.UserSaveRequest
import ru.hse.oros.model.request.entity.UserUpdateRequest
 import ru.hse.oros.model.response.UserResponse
 import ru.hse.oros.service.link.GroupStudentLinkService
 import ru.hse.oros.service.link.MaterialOwnerLinkService
 import ru.hse.oros.service.link.NotificationOwnerLinkService
 import ru.hse.oros.service.link.TaskOwnerLinkService
 import ru.hse.oros.utils.toUserResponse
 import java.util.*

@Service
class UserServiceImpl(
    @Autowired override val dao: UserDao,
    @Autowired @Lazy private val taskOwnerLinkService: TaskOwnerLinkService,
    @Autowired @Lazy private val materialOwnerLinkService: MaterialOwnerLinkService,
    @Autowired @Lazy private val notificationOwnerLinkService: NotificationOwnerLinkService,
    @Autowired @Lazy private val groupStudentLinkService: GroupStudentLinkService,
    @Autowired @Lazy private val solvedTaskService: SolvedTaskService
                     ) : AbstractEntityService<User>(), UserService {

    override val entityName: String
        get() = "User"

    override val logger
        get() = logger(this)


    override fun findAllToResponse(): List<Any> {
        logger.info("Find all ${entityName}s")
        return daoFindAll().toResponse()
    }

    override fun daoFindAll(): List<User> {
        return dao.findByOrderByName()
    }

    override fun findByIdToResponse(id: UUID): Any {
        return (findById(id) as User).toResponse()
    }

    override fun findByName(name: String): List<User> {
        return dao.findByName(name)
    }

    override fun findByNameToResponse(name: String): List<Any> {
        return (findByName(name)).toResponse()
    }

    override fun findByToken(token: String): User? {
        return dao.findByToken(token)
    }

    override fun findByTokenToResponse(token: String): Any {
        return (findByToken(token)
            ?: throw EntityNotFountException(entity = entityName)
                ).toResponse()
    }

    override fun create(request: EntityRequest): User {
        request as UserSaveRequest
        logger.info("Create new $entityName with name=${request.name}")

        if (dao.findByToken(request.token) != null || dao.findByEmail(request.email) != null)
            throw BadRequestException("User already exists")
        if (request.role == UserRole.SUPERVISOR) throw AccessDeniedException()

        return dao.save(
            User(
                token = request.token,
                name = request.name,
                role = request.role,
                email = request.email
                )
                       )
    }

    override fun createToResponse(request: UserSaveRequest): Any {
        return create(request).toResponse()
    }

    override fun update(id: UUID, request: EntityRequest) {
        logger.info("Update $entityName with id = $id")
        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        request as UserUpdateRequest

        val client = security.getClient()
        if (request.role == UserRole.SUPERVISOR) throw AccessDeniedException()
        if (request.role == UserRole.TEACHER
            && !(client.role == UserRole.TEACHER || client.role == UserRole.SUPERVISOR)) throw AccessDeniedException()
        if (request.role == UserRole.ASSISTANT
            && !(client.role == UserRole.TEACHER || client.role == UserRole.SUPERVISOR)) throw AccessDeniedException()

        if (security.isOROS(client.role)) {
            dao.save(newEntity(entity, request))
        } else if(client.id == id) {
            if (client.role != (request).role)
                throw AccessDeniedException(client.role.name)
            dao.save(newEntity(entity, request))
        } else {
            throw AccessDeniedException(client.role.name)
        }
    }

    override fun newEntity(entity: User, request: EntityRequest): User {
        request as UserUpdateRequest

        return entity.copy(
            name = request.name,
            role = request.role
                          )
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        if (entity.role == UserRole.SUPERVISOR) throw AccessDeniedException()

        taskOwnerLinkService.deleteOwner(id)
        materialOwnerLinkService.deleteOwner(id)
        notificationOwnerLinkService.deleteOwner(id)
        groupStudentLinkService.deleteStudent(id)
        solvedTaskService.deleteStudentTasks(id)

        dao.delete(entity)
    }

    private fun User.toResponse(): Any {
        return if (security.getClient().role == UserRole.SUPERVISOR)
            this
        else
            UserResponse(this)
    }

    private fun List<User>.toResponse(): List<Any> {
        return if (security.getClient().role == UserRole.SUPERVISOR)
            this
        else
            this.toUserResponse()
    }
}