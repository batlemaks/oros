package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.User
import ru.hse.oros.model.request.entity.UserSaveRequest
import java.util.*


interface UserService: EntityService<User> {
    fun findByName(name: String): List<User>
    fun findByNameToResponse(name: String): List<Any>
    fun findByToken(token: String): User?
    fun findByTokenToResponse(token: String): Any

    fun findAllToResponse(): List<Any>
    fun findByIdToResponse(id: UUID): Any
    fun createToResponse(request: UserSaveRequest): Any
}