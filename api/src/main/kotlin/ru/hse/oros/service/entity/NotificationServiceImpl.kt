package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.NotificationDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.Notification
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.model.request.entity.NotificationSaveRequest
import ru.hse.oros.service.link.NotificationOwnerLinkService
import java.util.*

@Service
class NotificationServiceImpl(
    @Autowired override val dao: NotificationDao,
    @Autowired @Lazy private val notificationOwnerLinkService: NotificationOwnerLinkService
                             ) : AbstractEntityService<Notification>(), NotificationService {

    override val entityName: String
    get() = "Notification"

    override val logger
    get() = logger(this)


    override fun daoFindAll(): List<Notification> {
        val client = security.getClient()

        val notifications = if (security.isOROS(client.role)) {
            dao.findDistinctByOrderByDateAsc().map { it.copy(is_viewed = true) }
        } else {
            dao.findStudentNotifications(client.id).map {
                val not = it[0] as Notification
                not.copy(is_viewed = it[1] as Boolean)
            }
        }

        return notifications
    }

    override fun create(request: NotificationSaveRequest, owner: UUID): Notification {
        return create(request, listOf(owner))
    }

    override fun create(request: NotificationSaveRequest, owners: List<UUID>): Notification {
        logger.info("Create new $entityName with author=${request.author}")
        val entity = dao.save(
            Notification(
                author = request.author,
                tags = request.tags,
                msg = request.msg,
                source = request.source
                        )
                       )
        notificationOwnerLinkService.create(entity.id, owners)
        return entity
    }

    override fun create(request: EntityRequest): Notification {
        request as NotificationSaveRequest
        logger.info("Create new $entityName with author=${request.author}")
        val entity = dao.save(
            Notification(
                author = request.author,
                tags = request.tags,
                msg = request.msg,
                source = request.source
                        )
                )
        request.links?.let {
            notificationOwnerLinkService.create(entity.id, it)
        }
        return entity
        }

    override fun newEntity(entity: Notification, request: EntityRequest): Notification {
        request as NotificationSaveRequest

        return entity.copy(
            author = request.author,
            tags = request.tags,
            msg = request.msg,
            source = request.source
                          )
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        notificationOwnerLinkService.deleteEntity(id)
        dao.delete(entity)
    }

    override fun delete(ids: List<UUID>) {
        logger.info("Delete ${entityName}s")

        for (id in ids) {
            dao.delete(id)
        }
    }

    override fun count(): Int {
        logger.info("Count $entityName's")

        val client = security.getClient()
        return dao.countUnviewed(client.id)
    }
}