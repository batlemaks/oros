package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.TaskSchemaDao
import ru.hse.oros.model.entity.TaskSchema
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.TaskSchemaSaveRequest
import ru.hse.oros.model.request.entity.TaskSchemaUpdateRequest
import ru.hse.oros.model.request.entity.EntityRequest

@Service
class TaskSchemaServiceImpl(
    @Autowired override val dao: TaskSchemaDao
                           ) : AbstractEntityService<TaskSchema>(), TaskSchemaService {

    override val entityName: String
        get() = "TaskSchema"

    override val logger
        get() = logger(this)


    override fun daoFindAll(): List<TaskSchema> {
        return dao.findByOrderByDateAsc()
    }

    override fun create(request: EntityRequest): TaskSchema {
        request as TaskSchemaSaveRequest
        logger.info("Create new $entityName with name=${request.name}")

        return dao.save(
            TaskSchema(
                name = request.name,
                description = request.description,
                type = request.type,
                category = request.category,
                options = request.options
                      )
                       )
    }

    override fun newEntity(entity: TaskSchema, request: EntityRequest): TaskSchema {
        request as TaskSchemaUpdateRequest

        return entity.copy(
            name = request.name?:let{entity.name},
            description = request.description,
            type = request.type?:let{entity.type},
            category = request.category?:let{entity.category},
            options = request.options
                          )
    }
}