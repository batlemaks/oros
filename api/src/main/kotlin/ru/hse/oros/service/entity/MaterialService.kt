package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.File
import ru.hse.oros.model.entity.Material
import java.util.*


interface MaterialService: EntityService<Material> {
    fun getFiles(id: UUID): List<File>
    fun getFilesToResponse(id: UUID): List<Map<String, Any?>>
}