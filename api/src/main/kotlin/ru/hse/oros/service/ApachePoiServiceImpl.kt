package ru.hse.oros.service

import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.streaming.SXSSFCell
import org.apache.poi.xssf.usermodel.*
import org.springframework.stereotype.Service
import ru.hse.oros.logger.logger
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.util.*


@Service
class ApachePoiServiceImpl : ApachePoiService {

    override val entityName: String
        get() = "ApachePoi"

    val logger = logger(this)

    private fun writeWorkbook(wb: XSSFWorkbook): ByteArray {
        val out = ByteArrayOutputStream()
        wb.write(out)

        out.close()
        wb.close()

        return out.toByteArray()
    }

    private fun writeWorkbooks(wbs: List<XSSFWorkbook>): List<ByteArray> {
        return wbs.map { writeWorkbook(it) }
    }

    override fun createGradebookReport(gradebook: Map<String, Any?>): ByteArray {
        val group = gradebook["group"] as Map<String, Any>
        val headers = gradebook["headers"] as List<Map<String, Any>>

        val wb = XSSFWorkbook()
        val sheet = wb.createSheet("Ведомость - ${group["name"]}")
        val headerRow = sheet.createRow(0)

        fillHeader(wb, sheet, headerRow, headers)

        val students = group["students"] as List<Map<String, Any>>
        var rowIndex = 1

        for (student in students) {
            fillRow(rowIndex++, wb, sheet, student, headers)
        }

        return writeWorkbook(wb)
    }

    private fun fillRow(
        rowIndex: Int,
        wb: XSSFWorkbook,
        sheet: XSSFSheet,
        values: Map<String, Any>,
        headers: List<Map<String, Any>>
                       ) {
        val row = sheet.createRow(rowIndex)
        val cellStyle = getCellStyle(wb)
        row.createCell(0).setCellValue(values["name"].toString())
        val marks = values["marks"] as List<Map<String, Any?>>

        for ((index, header) in headers.withIndex()) {
            val cell = row.createCell(index + 1)
            cell.cellStyle = cellStyle as XSSFCellStyle?

            marks.find { mark -> mark["id"] == header["id"] }
                ?.let {
                    it["mark"]?.let { cell.setCellValue(it as Double) }
                        ?: cell.setCellValue("")
                } ?: let { cell.setCellValue("") }
        }

    }

    private fun fillHeader(wb: XSSFWorkbook, sheet: XSSFSheet, row: XSSFRow, headers: List<Map<String, Any>>) {
        val headerStyle = getHeaderStyle(wb)
        row.createCell(0)

        var index = 1
        for (header in headers) {
            val headerCell = row.createCell(index)

            headerCell.cellStyle = headerStyle as XSSFCellStyle?
            headerCell.setCellValue(header["name"].toString())
            sheet.autoSizeColumn(index)

            index++
        }
    }

    private fun getHeaderStyle(wb: XSSFWorkbook): CellStyle {
        val headerFont = wb.createFont()
        headerFont.bold = true
        headerFont.fontHeightInPoints = 12.toShort()
        val headerStyle: CellStyle = getBorderedStyle(wb)
        headerStyle.alignment = HorizontalAlignment.CENTER
        headerStyle.setFont(headerFont)
        headerStyle.fillForegroundColor = IndexedColors.GREY_25_PERCENT.getIndex()
        headerStyle.fillPattern = FillPatternType.SOLID_FOREGROUND
        headerStyle.setFont(headerFont)

        return headerStyle
    }

    private fun getCellStyle(wb: XSSFWorkbook): CellStyle = getBorderedStyle(wb)

    private fun getBorderedStyle(wb: Workbook): CellStyle {
        val style = wb.createCellStyle()
        style.borderRight = BorderStyle.THIN;
        style.borderLeft = BorderStyle.THIN;
        style.borderTop = BorderStyle.THIN;
        style.borderBottom = BorderStyle.THIN;
        style.rightBorderColor = IndexedColors.BLACK.getIndex()
        style.bottomBorderColor = IndexedColors.BLACK.getIndex()
        style.leftBorderColor = IndexedColors.BLACK.getIndex()
        style.topBorderColor = IndexedColors.BLACK.getIndex()
        return style
    }


    override fun splitVariants(stream: InputStream): List<ByteArray> {
        val wb = XSSFWorkbook(stream)
        val sheet = wb.getSheetAt(0)
        val wbs = mutableListOf<XSSFWorkbook>()

        var rowIndex = 0
        val lastRowIndex = sheet.lastRowNum
        var wbCount = 0
        var varSheetRowIndex = 0
        var separator = 1

        while (true) {
            val row = sheet.getRow(rowIndex)

            if (isRowEmpty(row)) {
                ++separator
                varSheetRowIndex = 0
            } else {
                if (separator != 0) {
                    wbs.add(XSSFWorkbook()
                        .apply { createSheet("${sheet.sheetName} - Вариант ${++wbCount}") })
                }

                val newRow = wbs.last().getSheetAt(0).createRow(varSheetRowIndex++)

                for ((cellIndex, cell) in row.withIndex()) {
                    var newCell = newRow.createCell(cellIndex)
                    newCell = copyCellValue(cell, newCell)

                    val newStyle = wbs.last().createCellStyle()
                    newStyle.cloneStyleFrom(cell.cellStyle)
                    newCell.cellStyle = newStyle
                }

                separator = 0
            }

            if (separator == 3 || rowIndex == lastRowIndex) break

            ++rowIndex
        }

        return writeWorkbooks(wbs)
    }

    private fun copyCellValue(cell: Cell, newCell: XSSFCell): XSSFCell {
         when (cell.cellType) {
            CellType.NUMERIC -> if (DateUtil.isCellDateFormatted(cell)) {
                newCell.setCellValue(cell.dateCellValue)
            } else {
                newCell.setCellValue(cell.numericCellValue)
            }
            CellType.BOOLEAN -> newCell.setCellValue(cell.booleanCellValue)
            CellType.FORMULA -> newCell.cellFormula = cell.cellFormula
            else -> newCell.setCellValue(cell.stringCellValue)
        }
        return newCell
    }


    fun isRowEmpty(row: Row?): Boolean {
        var isEmpty = true
        val dataFormatter = DataFormatter()
        if (row != null) {
            for (cell in row) {
                if (dataFormatter.formatCellValue(cell).trim { it <= ' ' }.isNotEmpty()) {
                    isEmpty = false
                    break
                }
            }
        }
        return isEmpty
    }

}