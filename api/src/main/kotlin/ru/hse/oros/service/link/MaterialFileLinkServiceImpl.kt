package ru.hse.oros.service.link

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.hse.oros.dao.link.MaterialFileLinkDao
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.File
import ru.hse.oros.model.entity.Material
import ru.hse.oros.model.entity.link.MaterialFileLink
import ru.hse.oros.model.entity.link.MaterialFileLinkId
import ru.hse.oros.model.request.link.MaterialFileLinkRequest
import ru.hse.oros.service.entity.EntityService
import java.util.*

@Service
class MaterialFileLinkServiceImpl(
    @Autowired override val dao: MaterialFileLinkDao,
    @Autowired @Lazy override val entity1Service: EntityService<Material>,
    @Autowired @Lazy override val entity2Service: EntityService<File>
                                 ): AbstractLinkService<MaterialFileLink, MaterialFileLinkId, Material, File, MaterialFileLinkRequest>(), MaterialFileLinkService {

    override val entityName: String
        get() = "MaterialFileLink"

    override val logger
        get() = logger(this)

    override fun newLink(entity1Id: UUID, entity2Id: UUID): MaterialFileLink {
        return MaterialFileLink(entity1Id, entity2Id)
    }

    override fun deleteMaterial(id: UUID) = deleteEntity1(id)
    override fun deleteFile(id: UUID) = deleteEntity2(id)

}