package ru.hse.oros.service.entity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.hse.oros.dao.entity.TaskDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.Task
import ru.hse.oros.logger.logger
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.model.request.entity.TaskSaveRequest
import ru.hse.oros.model.request.entity.TaskUpdateRequest
import ru.hse.oros.service.link.TaskOwnerLinkService
import java.util.*

@Service
class TaskServiceImpl(
    @Autowired override val dao: TaskDao,
    @Autowired private val deadlineService: DeadlineService,
    @Autowired private val taskOwnerLinkService: TaskOwnerLinkService
                     ) : AbstractEntityService<Task>(), TaskService {

    override val entityName: String
        get() = "Task"

    override val logger
        get() = logger(this)


    override fun daoFindAll(): List<Task> {
        val client = security.getClient()

        return if (security.isOROS(client.role))
            dao.findDistinctByOrderByDateAsc()
        else
            dao.findStudentTasks(client.id)
    }

    override fun create(request: EntityRequest): Task {
        request as TaskSaveRequest
        logger.info("Create new $entityName with id's=${request.schema_id} & ${request.deadline_id}")

        val entity = dao.save(
            Task(
                schema_id = request.schema_id,
                deadline_id = request.deadline_id,
                name = request.name
                )
                       )

        request.links?.let {
            taskOwnerLinkService.create(entity.id, it)
        }

        return entity
    }

    override fun update(id: UUID, request: EntityRequest) {
        request as TaskUpdateRequest
        logger.info("Update $entityName with id = $id")

        val entity = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        entity.deadline_id?.let {
            if (it != request.deadline_id) deadlineService.delete(it)
        }

        dao.save(newEntity(entity, request))
    }

    override fun newEntity(entity: Task, request: EntityRequest): Task {
        request as TaskUpdateRequest

        return entity.copy(
            deadline_id = request.deadline_id
                          )
    }

    override fun delete(id: UUID) {
        logger.info("Delete $entityName with id = $id")

        val task = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)
        task.deadline_id?.let { deadlineService.delete(it) }
        taskOwnerLinkService.deleteEntity(id)

        dao.delete(task)
    }

    override fun addDeadline(taskId: UUID, deadlineId: UUID) {
        val task = dao.findByIdOrNull(taskId) ?: throw EntityNotFountException(taskId, entityName)
        dao.save(task.copy(deadline_id = deadlineId))
    }

    override fun deleteDeadline(taskId: UUID, deadlineId: UUID) {
        val task = dao.findByIdOrNull(taskId) ?: throw EntityNotFountException(taskId, entityName)
        dao.save(task.copy(deadline_id = null))
    }
}