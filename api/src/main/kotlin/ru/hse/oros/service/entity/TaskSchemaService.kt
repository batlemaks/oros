package ru.hse.oros.service.entity

import ru.hse.oros.model.entity.TaskSchema


interface TaskSchemaService: EntityService<TaskSchema> 