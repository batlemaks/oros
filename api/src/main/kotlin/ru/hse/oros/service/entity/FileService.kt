package ru.hse.oros.service.entity

import org.springframework.core.io.Resource
import org.springframework.web.multipart.MultipartFile
import ru.hse.oros.model.entity.File
import ru.hse.oros.model.request.entity.FileMaterialSaveRequest
import java.nio.file.Path
import java.util.*
import java.util.stream.Stream


interface FileService: EntityService<File> {
    fun loadAll(materialId: UUID): List<Resource>
    fun create(request: FileMaterialSaveRequest, file: MultipartFile): File
    fun store(file: MultipartFile): String
    fun loadFile(file: File): Resource
    fun loadById(id: UUID): Any
    fun splitVariants(file: MultipartFile): ByteArray
}