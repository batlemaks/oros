package ru.hse.oros.service.entity

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.context.annotation.Lazy
import ru.hse.oros.dao.entity.UserRequestDao
import ru.hse.oros.enums.UserRequestType
import ru.hse.oros.enums.UserRole
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.exception.ServiceException
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.UserRequest
import ru.hse.oros.model.request.entity.EntityRequest
import ru.hse.oros.model.request.entity.UserRequestRegisterRequest
import ru.hse.oros.model.request.entity.UserRequestSaveRequest
import ru.hse.oros.model.request.entity.UserSaveRequest
import java.util.*

@Service
class UserRequestServiceImpl(
    @Autowired override val dao: UserRequestDao,
    @Autowired @Lazy private val groupService: GroupService,
    @Autowired @Lazy private val userService: UserService
                            ) : AbstractEntityService<UserRequest>(), UserRequestService {

    override val entityName: String
    get() = "UserRequest"

    override val logger
    get() = logger(this)


    override fun findAll(): List<Any> {
        logger.info("Find all $entityName")
        return daoFindAll()
    }

    override fun daoFindAll(): List<UserRequest> {
        return dao.findByOrderByDateAsc()
    }

    override fun submit(id: UUID) {
        val userRequest = dao.findByIdOrNull(id) ?: throw EntityNotFountException(id, entityName)

        when (userRequest.type) {
            UserRequestType.ADD_TO_GROUP -> {
                val source = jacksonObjectMapper().readValue<MutableMap<String, Any?>>(
                    userRequest.source ?: throw ServiceException("Invalid user request data")
                                                                                      )
                groupService.addStudent(
                    groupId = UUID.fromString(source["groupId"].toString()),
                    studentId = userRequest.user_id!!
                                       )
            }
            UserRequestType.REGISTER_USER -> {
                val source = jacksonObjectMapper().readValue<MutableMap<String, Any?>>(
                    userRequest.source ?: throw ServiceException("Invalid user request data")
                                                                                      )
                userService.create(
                    UserSaveRequest(
                        token = source["token"].toString(),
                        name = source["name"].toString(),
                        role = UserRole.valueOf(source["role"].toString()),
                        email = source["email"].toString()
                                   )
                                  )
            }
            else -> throw ServiceException("Unknown user request type")
        }

        this.delete(id)
    }

    override fun requestNewUser(request: UserRequestRegisterRequest) {
        logger.info("Create new user registration $entityName")
        dao.save(
            UserRequest(
                type = request.type,
                source = request.source
                       )
                )
    }

    override fun create(request: EntityRequest): UserRequest {
        request as UserRequestSaveRequest
        logger.info("Create new $entityName from user with id = ${request.userId}")
        return dao.save(
            UserRequest(
                user_id = request.userId,
                type = request.type,
                source = request.source
                       )
                       )
    }

    override fun newEntity(entity: UserRequest, request: EntityRequest): UserRequest {
        request as UserRequestSaveRequest

        return entity.copy(
            source = request.source
                          )
    }
}