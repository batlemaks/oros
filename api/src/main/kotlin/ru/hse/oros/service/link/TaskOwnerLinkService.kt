package ru.hse.oros.service.link

import ru.hse.oros.model.entity.link.TaskOwnerLink


interface TaskOwnerLinkService: EntityOwnerLinkService<TaskOwnerLink>