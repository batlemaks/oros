package ru.hse.oros.service.link

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.hse.oros.dao.link.NotificationOwnerLinkDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.logger.logger
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.entity.Notification
import ru.hse.oros.model.entity.User
import ru.hse.oros.model.entity.link.GroupStudentLink
import ru.hse.oros.model.entity.link.NotificationOwnerLink
import ru.hse.oros.model.entity.link.NotificationOwnerLinkId
import ru.hse.oros.model.request.link.LinkRequest
import ru.hse.oros.model.request.link.NotificationOwnerLinkRequest
import ru.hse.oros.service.entity.NotificationService
import java.lang.RuntimeException
import java.util.*

@Service
class NotificationOwnerLinkServiceImpl(
    @Autowired override val dao: NotificationOwnerLinkDao,
    @Autowired @Lazy override val entityService: NotificationService
                                      ) : AbstractEntityOwnerLinkService<NotificationOwnerLink, NotificationOwnerLinkId, Notification, NotificationOwnerLinkRequest>(), NotificationOwnerLinkService {

    override val entityName: String
        get() = "NotificationOwnerLink"

    override val logger
        get() = logger(this)

    override fun newEntityOwnerLink(entityId: UUID, ownerId: UUID): NotificationOwnerLink {
        return NotificationOwnerLink(notification_id = entityId, owner_id = ownerId, is_viewed = true)
    }

    private fun newEntityOwnerLink(entityId: UUID, ownerId: UUID, isViewed: Boolean): NotificationOwnerLink {
        return NotificationOwnerLink(notification_id = entityId, owner_id = ownerId, is_viewed = isViewed)
    }

    override fun setIfViewed(notificationIds: List<UUID>, ownerID: UUID, isViewed: Boolean) {
        val owner = getOwner(ownerID) ?: throw EntityNotFountException(ownerID, "User")
        if (owner !is User) throw RuntimeException("$entityName view param set to a non user")

        dao.updateIsViewed(notificationIds, ownerID, isViewed)
    }

    override fun setIfViewed(notificationIds: List<UUID>, isViewed: Boolean) {
        val client = security.getClient()
        setIfViewed(notificationIds, client.id, isViewed)
    }

    override fun shareGroupNotifications(groupId: UUID, studentId: UUID) {
        val links = dao.findNotificationIdsByOwner(groupId).map {
            newEntityOwnerLink(it, studentId, false)
        }

        dao.saveAll(links)
    }

    override fun shareGroupNotifications(groupId: UUID, studentIds: List<UUID>) {
        val links = mutableListOf<NotificationOwnerLink>()
        val notificationIds = dao.findNotificationIdsByOwner(groupId)

        for (nId in notificationIds)
            links.addAll(studentIds.map { sId ->
                newEntityOwnerLink(nId, sId, false)
            })

        dao.saveAll(links)
    }

    override fun confiscateGroupNotifications(groupId: UUID, studentId: UUID) {
        dao.deleteGroupStudents(groupId, studentId)
    }

    override fun confiscateGroupNotifications(groupId: UUID, studentIds: List<UUID>) {
        dao.deleteGroupStudents(groupId, studentIds)
    }

    override fun set(id: UUID, entityIds: List<UUID>) {
        logger.info("Set new $entityName with link list and entity id = $id")
        entityService.findById(id)

        val oldOwners = dao.findOwnersByNotificationId(id)
        var newOwners = entityIds as MutableList<UUID>
        var links = mutableListOf<NotificationOwnerLink>()

        for (eId in entityIds) {
            val owner = getOwner(eId) ?: throw EntityNotFountException(eId, "Group/User")

            when (owner) {
                is Group -> {
                    val students = groupService.getStudentsIds(owner.id)
                    newOwners.addAll(students)
                    links.add(newEntityOwnerLink(id, eId, true))
                    links.addAll(students.map {
                        newEntityOwnerLink(id, it, false)
                    })
                }
                is User -> {
                    links.add(newEntityOwnerLink(id, eId, false))
                }
                else -> {
                    throw RuntimeException("Unknown owner type")
                }
            }
        }

        newOwners = newOwners.distinct().toMutableList()
        links = links.filter { !oldOwners.contains(it.owner_id) }.distinctBy { it.owner_id }.toMutableList()

        dao.delete(id, oldOwners.minus(newOwners))
        dao.saveAll(links)
    }

    override fun create(request: LinkRequest) {
        request as NotificationOwnerLinkRequest
        logger.info("Create new $entityName with entity id = ${request.entity_id} & owner id = ${request.owner_id}")

        val notification = entityService.findById(request.entity_id) as Notification
        val owner = getOwner(request.owner_id) ?: throw EntityNotFountException(request.owner_id, "Group/User")
        val oldOwners = dao.findOwnersByNotificationId(notification.id)

        when (owner) {
            is Group -> {
                val links = groupService.getStudentsIds(owner.id).map {
                    newEntityOwnerLink(notification.id, it, false)
                }.filter { !oldOwners.contains(it.owner_id) }
                dao.saveAll(links)

                if (!oldOwners.contains(owner.id))
                    dao.save(newEntityOwnerLink(notification.id, owner.id, true))
            }
            is User -> {
                dao.save(newEntityOwnerLink(notification.id, request.owner_id))
            }
            else -> {
                throw RuntimeException("Unknown owner type")
            }
        }

    }

    override fun create(id: UUID, entityIds: List<UUID>) {
        logger.info("Create new $entityName with link list and entity id = $id")

        val notification = entityService.findById(id) as Notification
        var links = mutableListOf<NotificationOwnerLink>()
        val oldOwners = dao.findOwnersByNotificationId(id)

        for (ownerId in entityIds) {
            val owner = getOwner(ownerId) ?: throw EntityNotFountException(ownerId, "Group/User")

            when (owner) {
                is Group -> {
                    links.add(newEntityOwnerLink(id, ownerId, true))
                    links.addAll(
                        groupService.getStudentsIds(ownerId).map {
                            newEntityOwnerLink(id, it, false)
                        })
                }
                is User -> {
                    links.add(newEntityOwnerLink(id, ownerId, false))
                }
                else -> {
                    throw RuntimeException("Unknown owner type")
                }
            }
        }

        links = links.filter { !oldOwners.contains(it.owner_id) }.distinctBy { it.owner_id }.toMutableList()
        dao.saveAll(links)
    }

    override fun delete(request: LinkRequest) {
        request as NotificationOwnerLinkRequest
        logger.info("Delete $entityName with entity id = ${request.entity_id} & owner id = ${request.owner_id}")

        val notification = entityService.findById(request.entity_id) as Notification
        val owner = getOwner(request.owner_id) ?: throw EntityNotFountException(request.owner_id, "Group/User")

        when (owner) {
            is Group -> {
                val students = groupService.getStudentsIds(owner.id)
                dao.delete(notification.id, students)
                dao.delete(notification.id, owner.id)
            }
            is User -> {
                dao.delete(notification.id, owner.id)
            }
            else -> {
                throw RuntimeException("Unknown owner type")
            }
        }
    }

    override fun delete(id: UUID, entityIds: List<UUID>) {
        logger.info("Delete ${entityName}s with link list and entity id = $id")

        val notification = entityService.findById(id) as Notification
        val owners = mutableListOf<UUID>()

        for (ownerId in entityIds) {
            val owner = getOwner(ownerId) ?: throw EntityNotFountException(ownerId, "Group/User")

            when (owner) {
                is Group -> {
                    val students = groupService.getStudentsIds(owner.id)
                    owners.addAll(students)
                    owners.add(owner.id)
                }
                is User -> {
                    owners.add(owner.id)
                }
                else -> {
                    throw RuntimeException("Unknown owner type")
                }
            }
        }

        dao.delete(notification.id, owners)
    }

    override fun deleteOwner(id: UUID) {
        logger.info("Delete $entityName with owner id = $id")

        val owner = getOwner(id) ?: throw EntityNotFountException(id, "Group/User")

        when (owner) {
            is Group -> {
                dao.deleteGroupStudents(id)
                dao.deleteEntity2(id)
            }
            is User -> {
                dao.deleteEntity2(id)
            }
            else -> {
                throw RuntimeException("Unknown owner type")
            }
        }
    }

}