package ru.hse.oros.service.link

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import ru.hse.oros.dao.link.LinkDao
import ru.hse.oros.exception.EntityNotFountException
import ru.hse.oros.model.entity.AbstractEntity
import ru.hse.oros.model.entity.link.Link
import ru.hse.oros.model.entity.link.LinkId
import ru.hse.oros.model.request.link.EntitiesLinkRequest
import ru.hse.oros.model.request.link.LinkRequest
import ru.hse.oros.security.SecurityService
import ru.hse.oros.service.entity.EntityService
import java.util.*


abstract class AbstractLinkService<T: Link, ID: LinkId, E1: AbstractEntity, E2: AbstractEntity, R: EntitiesLinkRequest>: LinkService<T> {
    @Autowired
    protected lateinit var security: SecurityService

    protected abstract val logger: Logger
    protected abstract val dao: LinkDao<T, ID>

    protected abstract val entity1Service: EntityService<E1>
    protected abstract val entity2Service: EntityService<E2>


    override fun set(id: UUID, entityIds: List<UUID>) {
        logger.info("Set new $entityName with link list and main entity id = $id")

        entity1Service.findById(id)
        val links = mutableListOf<T>()

        for (eId in entityIds) {
            entity2Service.findById(eId)
            links.add(newLink(id, eId))
        }

        dao.deleteEntity1(id)
        dao.saveAll(links)
    }

    override fun create(id: UUID, entityIds: List<UUID>) {
        logger.info("Create new $entityName with link list and main entity id = $id")

        entity1Service.findById(id)
        val links = mutableListOf<T>()

        for (eId in entityIds) {
            entity2Service.findById(eId)
            links.add(newLink(id, eId))
        }

        dao.saveAll(links)
    }

    protected abstract fun newLink(entity1Id: UUID, entity2Id: UUID): T

    override fun create(request: LinkRequest) {
        request as R
        logger.info("Create new $entityName with entity1 id = ${request.entity1_id} & entity2 id = ${request.entity2_id}")

        entity1Service.findById(request.entity1_id)
        entity2Service.findById(request.entity2_id)

        dao.save(newLink(request.entity1_id, request.entity2_id))
    }

    override fun delete(request: LinkRequest) {
        request as R
        logger.info("Delete $entityName with entity1 id = ${request.entity1_id} & entity2 id = ${request.entity2_id}")

        entity1Service.findById(request.entity1_id)
        entity2Service.findById(request.entity2_id)

        dao.delete(request.entity1_id, request.entity2_id)
    }

    override fun delete(id: UUID, entityIds: List<UUID>) {
        logger.info("Delete ${entityName}s with link list and entity id = $id")

        entity1Service.findById(id)
        for (eId in entityIds) {
            entity2Service.findById(eId)
        }

        dao.delete(id, entityIds)
    }

    override fun deleteEntity1(id: UUID) {
        logger.info("Delete $entityName with entity1 id = $id")

        entity1Service.findById(id)
        dao.deleteEntity1(id)
    }

    override fun deleteEntity2(id: UUID) {
        logger.info("Delete $entityName with entity2 id = $id")

        entity2Service.findById(id)
        dao.deleteEntity2(id)
    }
}