package ru.hse.oros.controller

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.exchange
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import ru.hse.oros.dao.entity.UserRequestDao
import ru.hse.oros.enums.UserRequestType
import ru.hse.oros.enums.UserRole
import ru.hse.oros.model.entity.Group
import ru.hse.oros.model.entity.User
import ru.hse.oros.model.entity.UserRequest
import ru.hse.oros.model.request.entity.GroupSaveRequest
import ru.hse.oros.model.request.entity.UserRequestSaveRequest
import ru.hse.oros.model.request.entity.UserSaveRequest
import ru.hse.oros.model.response.UserResponse
import ru.hse.oros.service.entity.GroupService
import ru.hse.oros.service.entity.UserRequestService
import ru.hse.oros.service.entity.UserService


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserRequestTests(
    @Autowired val restTemplate: TestRestTemplate,
    @Autowired val service: UserRequestService,
    @Autowired val userService: UserService,
    @Autowired val groupService: GroupService,
    @Autowired val dao: UserRequestDao
                      ): ControllerTest() {


    private var user = User(
        token = "testUserRequest001",
        name = "test UserRequest user 001",
        role = UserRole.STUDENT,
        email = "testUserRequest_mail1@hse.ru"
                           )

    private var group = Group(
        name = "test UserRequest group 001",
        events = null
                             )

    private var userRequest = UserRequest(
        type = UserRequestType.ADD_TO_GROUP,
        user_id = user.id,
        source = ObjectMapper().writeValueAsString(mapOf("groupId" to group.id))
                                         )


    init {
        group = groupService.create(
            GroupSaveRequest(
                name = group.name,
                events = group.events
                            )
                                   ).copy()

        user = userService.create(
            UserSaveRequest(
            token = user.token,
            name = user.name,
            role = user.role,
            email = user.email
                           )
                                 ).copy()

        userRequest = service.create(
            UserRequestSaveRequest(
                userId = user.id,
                type = userRequest.type,
                source = ObjectMapper().writeValueAsString(mapOf("groupId" to group.id))
                                  )
                                    ).copy()
    }


    @Test
    fun `submit user request`() {
        val request = HttpEntity<Any?>(
            HttpHeaders().apply { add("Cookie", "token=$supervisorToken") })

        val count = dao.findAll().count()

        with(restTemplate.exchange<String>("/requests/submit/${userRequest.id}", HttpMethod.POST, request)) {
            assertEquals(HttpStatus.OK, this.statusCode)
        }

        assertTrue(dao.findAll().count() == count - 1)
        assertNull(dao.findByIdOrNull(userRequest.id))

        val u = ((groupService.findByIdWithStudents(group.id) as Map<String, Any?>)["students"] as List<UserResponse>)[0]
        assertEquals(user.id, u.id)
    }
}