/** @fileOverview
 * @author Sidorov Maxim
 * @version 0.0.3
 */

import React from "react";
import ReactDOM from "react-dom";
import App from "./ui/app";

import './ui/utils/global-styles/css-imports'
import {MsalProvider} from "@azure/msal-react";
import {PublicClientApplication} from "@azure/msal-browser";
import {msalConfig} from "./../auth/authConfig";

const msalInstance = new PublicClientApplication(msalConfig);

ReactDOM.render(<MsalProvider instance={msalInstance}><App /></MsalProvider>, document.getElementById("root"));