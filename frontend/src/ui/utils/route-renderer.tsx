import {RouteType} from "./types/route-types";
import React, {memo} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import ErrorPage from "../routes/home-page/error-page";

type Props = {
    routes?: RouteType[],
    path?: string,
    error?: boolean,
    redirect?: string,
}

const routeRendererFC: React.FC<Props> = ({routes, path = '/', error = false, redirect}:Props) => {
    return (
        <Switch>
            {
                routes?.map((item, index) =>
                    <Route
                        key={item.title + index.toString()}
                        exact={item.isExact}
                        path={path + item.path}
                    >
                        {
                            item.component && <item.component />
                        }
                    </Route>
                )
            }
            {
                error && <Route component={ErrorPage}/>
            }
            {
                redirect && <Redirect to={redirect} />
            }
        </Switch>
    )
}

const RouteRenderer = memo(routeRendererFC)

export default RouteRenderer