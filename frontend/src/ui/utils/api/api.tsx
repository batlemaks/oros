import {Dispatch, Store} from "redux";
import {State} from "../../store/reducers/reducer";
import {IUserApi, UserApi} from "./user-api";
import {User} from "../types/user-types";
import {getCookies, setCookies} from "../cookies";
import {BACKEND_URL} from "../constants/constants";
import {useMsal} from "@azure/msal-react";

// require('user-api')

export class Api {
    private readonly _store:Store<State>
    private readonly _dispatch:Dispatch
    private readonly _userApi: IUserApi;

    private user:User = {
        isAuthorized:false
    }

    constructor(store: Store<State>) {
        this._store = store
        this._dispatch = store.dispatch
        this._userApi = new UserApi(this.user)

        this.init()
    }

    getStore():Store<State> {
        return this._store
    }

    getCookies(name:string):string|undefined {
        return getCookies(name)
    }

    setCookies(name:string, value:any, options:{[k:string]:string|boolean|number} = {}):void {
        return setCookies(name, value, options)
    }

    // deleteCookies(name:string):void{
    //     return deleteCookie(name)
    // }

    private init() {
        //let cookie = this._userApi.getUserCookies()

        this._userApi.getUserCookies()
    }

    async get(path: string, params: any = {}) {
        const answer = await fetch(`${BACKEND_URL}api/v1/${path}`, {
            ...params,
            credentials:'include' ,
        }).catch((e) => {
            throw new Error(e);
        });
        if (answer.status === 403) {
            const { instance } = useMsal();

            await instance.logout()
        }

        let res:any = await answer.text();

        try {
            res = JSON.parse(res) || {}
        } catch {
            res = {}
        }

        if (String(answer.status)[0] !== '2' && String(answer.status)[0] !== '3') {
            throw new Error(res.message);
        }
        return res;
    }

    async post(url:string, data?:any, headers?:any) {
        const response = await this.get(url,{
            method: 'POST',
            headers: headers ? headers : {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: data && JSON.stringify(data)
        })

        return response
    }

    async delete(url:string) {
        const response = await this.get(url,{
            method: 'DELETE',
        })

        return response
    }

    async put(url:string, data:any) {
        const response = await this.get(url,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        })

        return response
    }
}



