export const SUPPORTED_LANGUAGES: {
    [k:string]:string
} = {
    RU:'RU',
    ENG:'ENG',
}