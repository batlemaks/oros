export const PAGE_PERMISSIONS : {

} = {
    homePage: {

    },
}

export enum UserRole {
    initial,
    student,
    assistant,
    teacher,
    admin
}