import React, {memo} from "react";

import './info.scss'

const infoCourseFC:React.FC = () => {
    return (
        <div className={'info'}>
            <h1 className={'info__h1'}>Исследование операций</h1>
            <div className={'info__data'}>
                <div className={'info__row'}>
                    <span className={'info__item-header'}>Департамент: </span>
                    <a className={'info__item-content'} href={'https://cs.hse.ru/dse/'}>Департамент программной
                        инженерии</a>
                </div>
                <div className={'info__row'}>
                    <span className={'info__item-header'}>ПУД: </span>
                    <a className={'info__item-content'}
                       href={'https://lms.hse.ru/index.php?page=discipline_programs&page_point=summary&dp_id=784906'}>Исследование
                        операций</a>
                </div>
                <div className={'info__row'}>
                    <span className={'info__item-header'}>Преподаватель: </span>
                    <a className={'info__item-content'} href={'https://www.hse.ru/staff/gzhukova'}>Жукова Галина
                        Николаевна</a>
                </div>
            </div>
        </div>
    )
}

const InfoCourse = memo(infoCourseFC)

export default InfoCourse;