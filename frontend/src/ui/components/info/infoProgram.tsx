import React, {memo} from "react";

import './info.scss'

const infoProgramFC:React.FC = () => {
    return (
        <div className={'info'}>
            <h1 className={'info__h1'}>Исследование операций</h1>
            <div className={'info__data'}>
                <div className={'info__text'}>
                    Программа является результатом дипломной работы студентов 4-го курса БПИ. //Дополнить
                </div>
            </div>
        </div>
    )
}

const InfoProgram = memo(infoProgramFC)

export default InfoProgram;