import React, {memo, useEffect, useState} from "react";
import ContentWrapper from "../content-wrapper/content-wrapper";
import {ArrowLeftIcon} from "@modulz/radix-icons";

import './task-creator.scss'
import './task.scss'
import {Group} from "../table/table";
import {useMessageContext} from "../../routes/home-page/home-page";
import {useApiContext} from "../../utils/contexts/api-context";

export enum TaskType {
    test='TEST',
    xlsx='XLSX',
    python='PYTHON'
}

export enum PythonTaskType {
    singleTask='SINGLE', //Один файл с условиями для всех
    singlePlusPersonalData='SINGLE_PERSONAL', //Один файл с условиями + персональные исходники
    PersonalData='PERSONAL'
}

export enum XlsxTaskType {
    singleTask='SINGLE', //Один файл с условиями для всех
    singlePlusPersonalData='SINGLE_PERSONAL', //Один файл с условиями + персональные исходники
    PersonalData='PERSONAL',
    sliceData='SLICE'
}

export enum WorkType {
    init='INIT',
    homeWork='HOMEWORK',
    test='TEST',
}

export const workTypes = [
    {
        type:WorkType.init,
        value:  'Выберите тип работы'
    },
    {
      type:WorkType.homeWork,
      value:  'Домашняя работа'
    },
    {
        type:WorkType.test,
        value:  'Тест'
    },
]

export type StudentTaskType = {
    id:string,
    name?:string,
    options?:{
        [k:string]:any,
    },
    type?: TaskType,
    links?: string[] | string,
    description?: string,
    isCompleted?:boolean
    date?: any
}

const taskCreatorFC:React.FC = () => {
    document.title = "ИО - Редактор заданий"

    const [tasks, setTasks] = useState<StudentTaskType[] | undefined>(undefined)
    const [editMode, setEditMode] = useState<number>(-1)
    const [publicMode, setPublicMode] = useState<number>(-1)
    const [isCompleted, setIsCompleted] = useState(false)

    const [allGroups,setAllGroup] = useState<Group[]>([])

    const [currentGroups, setCurrentGroups] = useState<string[]>()
    const [currentWorkType, setCurrentWorkType] = useState<WorkType>()
    const [currentDeadlineDate, setCurrentDeadlineDate] = useState()
    const [currentDeadlineTime, setCurrentDeadlineTime] = useState()
    const [currentCoefficient,setCurrentCoefficient] = useState(1)

    const startCreateMode = () => {
        setCurrentTask({
            id: '0',
            name: 'ВВедите название',
            description: 'Введите описание'
        })
        setEditMode(1)
        setIsCompleted(false)
    }

    const [currentTask, setCurrentTask] = useState<StudentTaskType>()
    const {setSuccess, setError} = useMessageContext() || {}
    const api = useApiContext()

    const openTaskById = (id: string) => {
        const task = tasks?.find(task => task.id == id)

        setIsCompleted(task?.isCompleted || false)
        setCurrentTask(task)
        setEditMode(0)
    }

    const toggleEditMode = (num: number, callBack?: (() => void) | any) => {
        setEditMode(num)

        setIsCompleted(num == -2)

        callBack&&callBack()
    }

    const togglePublicMode = (num: number, event:any) => {
        event.preventDefault()

        setPublicMode(num)
    }

    const toggleTestEditMode = (num: number, event:any) => {
        event.preventDefault()

        setIsCompleted(num == -2)

        const test: { [k:string]:string }[] = currentTask?.options && currentTask?.options['test']

        if (test && test[test.length-1].question && test[test.length-1].answer) {
            test.push({})
            setEditMode(num)
        }
    }

    const onChangeTask = (item: string, val?: { target: { value: any } }, callBack?: () => void) => {
        setCurrentTask({...(currentTask || {id: '-'}), [item]: (val?.target.value || '-')})

        callBack && callBack()
    }

    const setPublicState = (method: (_:any) => any, val?: { target: { value: any } }) => {
        console.log(val)

        method(val?.target?.value)
    }

    const onChangeTaskOption = (item: string,callBack?: (() => void) | null, val?: { target: { value: string } }) => {
        setCurrentTask(
            {
                ...(currentTask || {id: '-'}),
                options: {
                    // ...currentTask?.options,
                    [item]: (val?.target.value || '-')
                }
            })

        callBack && callBack()
    }

    const onChangeTaskOptionWithSave = (item: string,callBack?: (() => void) | null, val?: { target: { value: string } }) => {
        setCurrentTask(
            {
                ...(currentTask || {id: '-'}),
                options: {
                     ...currentTask?.options,
                    [item]: (val?.target.value || '-')
                }
            })

        callBack && callBack()
    }

    const deleteFileByName = (place: string, name: string) => {
        let files = currentTask?.options && currentTask?.options[place].filter((item: {
            file: { name: string }
        }) => {
            return item.file.name != name
        })

        if (files.length == 0)
            files = null

        setCurrentTask(
            {
                ...(currentTask || {id: '-'}),
                options: {
                    ...currentTask?.options,
                    [place]: files
                }
            })
    }

    const addTestTask = (key:string,val?: { target: { value: string } }) => {
        let test: { [k:string]:string }[] = currentTask?.options && currentTask?.options['test']

        if (test) {
            test[test.length-1] = {...test[test.length-1],[key]:val?.target?.value||''}
        } else {
            test = [{[key]:val?.target?.value||''}]
        }

        setCurrentTask(
            {
                ...(currentTask || {id: '-'}),
                options: {
                    ...currentTask?.options,
                    test: test
                }
            })
    }

    const deleteTestTaskByIndex = (testIndex: number) => {
        let test = currentTask?.options && currentTask?.options['test']
            .filter((test:{question:string,answer:string}, index:number) => !(index == testIndex)
        )

        if (test.length == 0)
            test = null

        setCurrentTask(
            {
                ...(currentTask || {id: '-'}),
                options: {
                    ...currentTask?.options,
                    test: test
                }
            })
    }

    const saveTask =() => {
        if (currentTask?.date) {
            api?.put(`taskschemas/${currentTask.id}`,{
                ...currentTask,
                options: JSON.stringify(currentTask?.options || {}),
                category:'TEST',
                time: Date.now()
            }).then(() => {
                setSuccess && setSuccess()
            }).catch((e) => {
                setError && setError()
            }).then(() => {
                toggleEditMode(-2)
                updateTasks()
            })
        } else {
            api?.post('taskschemas',{
                ...currentTask,
                options: JSON.stringify(currentTask?.options || {}),
                category:'TEST',
                time: Date.now()
            }).then(() => {
                setSuccess && setSuccess()
            }).catch((e) => {
                setError && setError()
            }).then(() => {
                toggleEditMode(-2)
                updateTasks()
            })
        }
    }

    const deleteTack = () => {
        api?.delete(`taskschemas/${currentTask?.id}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            toggleEditMode(-1)
            updateTasks()
        })
    }

    const uploadFilesOption = (item: string,  val?: any,callBack?:()=>void) => { //{ target: { value?: string, files: FileList } }
        const files = Array.from(val?.target.files).map((item: any) => {
            const form_data = new FormData();
            val?.target.files[0] && form_data.append(item.name, item)
            return {
                file: item,
                form_data: form_data
            }
        })

        setEditMode(0)

        setCurrentTask(
            {
                ...(currentTask || {id: '-'}),
                options: {
                    ...currentTask?.options,
                    [item]: files
                }
            })

        callBack&&callBack()
    }

    const setPublicStateGroups = (method: (_:any) => any, val?: { target: { options: any } }) => {
        const result = [];

        const options = val?.target?.options;
        let opt;

        for (let i=0, iLen=options.length; i<iLen; i++) {
            opt = options[i];

            if (opt.selected) {
                result.push(opt.value || opt.text);
            }
        }

        method(result)
    }

    const fetchData = async () => {
        const data: {
            tasks: StudentTaskType[],
            groups: Group[]
        } = {
            tasks: [],
            groups: []
        }

        await api?.get('taskschemas').then((res) => {
            console.log(data)
            data.tasks = res.map((item:any) => {
                    return {
                        ...item,
                        options: JSON.parse(item.options),
                        isCompleted: true,
                    }
                }
            ).reverse()
        })

        await api?.get('groups').then((res) => {
            data.groups = res
        })

        await api?.get('users').then((res) => {
            data.groups = [
                ...data.groups,
                ...res
                ]
        })

        return data
    }

    const publish = () => {
        const schemaId = currentTask && tasks?.find((item) =>
            item.description === currentTask.description &&
            item.name === currentTask.name &&
            item.type === currentTask.type
        )?.id

        if (!(currentTask && currentDeadlineTime && currentDeadlineDate)) {
            setError && setError()
        } else {
            api?.post('tasks', {
                schemaId,
                links: currentGroups,
                name: currentTask?.name
            }).catch(() => {
                setError && setError()
            }).then(() =>
                api.get('tasks')
            ).then((res) => {
                const taskId = res.find((item: any) => {
                    return item.schema_id === schemaId &&
                        item.name === currentTask?.name
                })?.id

                return Promise.all([
                    api?.post('deadlines', {
                        taskId,
                        name: currentTask?.name,
                        type: currentWorkType,
                        time: Date.parse(`${currentDeadlineDate}T${currentDeadlineTime}:00.000+03:00`)
                    }),
                    api?.post(`tasks/${taskId}/students`,
                        currentGroups
                    )
                ])
            }).then(() => {
                setSuccess && setSuccess()
            }).catch(() => {
                setError && setError()
            })
        }
    }

    const updateTasks = () => [
        fetchData().then((data) => {
            setTasks(data.tasks)
            setAllGroup(data.groups)
        })
    ]

    useEffect(() => {
        updateTasks()
    }, [])

    return (
        <ContentWrapper minWidth={1300} maxWidth={1500} maxHeight={800}>
            <article className={'task-creator'}>
                <header className={'task-creator__header'}>
                    <div className={'task-creator__info'}>
                        <h2 className={'task-creator__h2'}>
                            Редактор заданий
                        </h2>
                    </div>
                </header>
                <section className={'task-creator__area'}>
                    <div className={'task-list'}>
                        <header className={'task-list__header'}>
                            <h3 className={'task-list__h3'}>Задания</h3>
                        </header>
                        <div className={'task-list__tasks'}>
                            <div className={'task__plus'} onClick={startCreateMode}>
                                +
                            </div>
                            {
                                tasks?.map((task, taskIndex) =>
                                    <div key={taskIndex} className={'task'} onClick={openTaskById.bind(null, task.id)}>
                                        <div className={'task__back'}>{task.type}</div>
                                        <span className={'task__name'}>{task.name}</span>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                    {
                        editMode != -1 &&
                        <div className={'task__content'}>
                            <header className={'task__header'}>
                                <h3 className={'task__h3'}>
                                    {
                                        currentTask ?
                                            <><div><b>Задание: </b>{currentTask.name}</div></> :
                                            <b>Новое задание</b>
                                    }
                                </h3>
                                {
                                    currentTask?.date && <button className={'button'} onClick={deleteTack}>Удалить</button>
                                }
                            </header>
                            <div className={'task__data'}>
                                {
                                    currentTask?.name && editMode != 1 &&
                                    <div className={'task__item'}>
                                        <span className={'task-item__name'}>Название: </span>
                                        <span className={'task-item__value'}>{currentTask.name}</span>
                                        <button className={'button'} onClick={toggleEditMode.bind(null, 1,null)}>
                                            Изменить
                                        </button>
                                    </div>
                                }
                                {
                                    editMode == 1 &&
                                    <form className={'task__item'} onSubmit={toggleEditMode.bind(null, 0,null)}>
                                        <span className={'task-item__name'}>Введите название: </span>
                                        <input type={'text'} defaultValue={currentTask?.name} required={true}
                                               onChange={onChangeTask.bind(null, 'name')}/>
                                        <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                    </form>
                                }
                                {
                                    <div className={'task__item'}>
                                        <div className={'task-item__scroll'}>
                                            <div
                                                className={`task ${currentTask?.type === TaskType.python && '_active'}`}
                                                onClick={onChangeTask.bind(
                                                    null,
                                                    'type',
                                                    {target: {value: TaskType.python}},
                                                    toggleEditMode.bind(null, 0,null)
                                                )}>
                                                <div className={'task__back'}>PYTHON</div>
                                                <span className={'task__name'}>Программирование</span>
                                            </div>
                                            <div className={`task ${currentTask?.type === TaskType.xlsx && '_active'}`}
                                                 onClick={onChangeTask.bind(
                                                     null,
                                                     'type',
                                                     {target: {value: TaskType.xlsx}},
                                                     toggleEditMode.bind(null, 0,null)
                                                 )}>
                                                <div className={'task__back'}>XLSX</div>
                                                <span
                                                    className={'task__name'}>Заполнение таблицы</span>
                                            </div>
                                            <div className={`task ${currentTask?.type === TaskType.test && '_active'}`}
                                                 onClick={onChangeTask.bind(
                                                     null,
                                                     'type',
                                                     {target: {value: TaskType.test}},
                                                     toggleEditMode.bind(null, 0,null)
                                                 )}>
                                                <div className={'task__back'}>TEST</div>
                                                <span className={'task__name'}>Тест</span>
                                            </div>
                                        </div>
                                    </div>
                                }
                                {
                                    currentTask?.description && editMode != 3 &&
                                    <div className={'task__item'}>
                                        <span className={'task-item__name'}>Описание: </span>
                                        <span className={'task-item__value'}>{currentTask.description}</span>
                                        <button className={'button'} onClick={toggleEditMode.bind(null, 3,null)}>
                                            Изменить
                                        </button>
                                    </div>
                                }
                                {
                                    (editMode == 3 || !currentTask?.description) &&
                                    <form className={'task__item'} onSubmit={toggleEditMode.bind(null, 0,null)}>
                                        <span className={'task-item__name'}>Введите описание: </span>
                                        <input type={'text'} defaultValue={currentTask?.description}
                                               onChange={onChangeTask.bind(null, 'description')} required={true}/>
                                        <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                    </form>
                                }
                                {

                                    <>
                                        {
                                            currentTask?.type == TaskType.python &&
                                            <div className={'task__item _option'}>
                                                <div className={'task-item__scroll'}>
                                                    <div
                                                        className={`task ${currentTask?.options?.pythonType === PythonTaskType.singleTask && '_active'}`}
                                                        onClick={onChangeTaskOption.bind(
                                                            null,
                                                            'pythonType',
                                                            toggleEditMode.bind(null, 0,null),
                                                            {target: {value: PythonTaskType.singleTask}}
                                                        )}>
                                                        <div className={'task__back'}>PYTHON</div>
                                                        <span className={'task__name'}>Условия единые для всех</span>
                                                    </div>
                                                    <div
                                                        className={`task ${currentTask?.options?.pythonType === PythonTaskType.singlePlusPersonalData && '_active'}`}
                                                        onClick={onChangeTaskOption.bind(
                                                            null,
                                                            'pythonType',
                                                            toggleEditMode.bind(null, 0,null),
                                                            {target: {value: PythonTaskType.singlePlusPersonalData}}
                                                        )}>
                                                        <div className={'task__back'}>PYTHON</div>
                                                        <span
                                                            className={'task__name'}>Условия, личные исходники</span>
                                                    </div>
                                                    <div
                                                        className={`task ${currentTask?.options?.pythonType === PythonTaskType.PersonalData && '_active'}`}
                                                        onClick={onChangeTaskOption.bind(
                                                            null,
                                                            'pythonType',
                                                            toggleEditMode.bind(null, 0,null),
                                                            {target: {value: PythonTaskType.PersonalData}}
                                                        )}>
                                                        <div className={'task__back'}>PYTHON</div>
                                                        <span
                                                            className={'task__name'}>Личные условия</span>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        {
                                            currentTask?.type == TaskType.xlsx &&
                                            <>
                                                <div className={'task__item _option'}>
                                                    <div className={'task-item__scroll'}>
                                                        <div
                                                            className={`task ${currentTask?.options?.xlsxType === XlsxTaskType.singleTask && '_active'}`}
                                                            onClick={onChangeTaskOption.bind(
                                                                null,
                                                                'xlsxType',
                                                                toggleEditMode.bind(null, 0,null),
                                                                {target: {value: XlsxTaskType.singleTask}}
                                                            )}>
                                                            <div className={'task__back'}>XLSX</div>
                                                            <span
                                                                className={'task__name'}>Условия единые для всех</span>
                                                        </div>
                                                        <div
                                                            className={`task ${currentTask?.options?.xlsxType === XlsxTaskType.singlePlusPersonalData && '_active'}`}
                                                            onClick={onChangeTaskOption.bind(
                                                                null,
                                                                'xlsxType',
                                                                toggleEditMode.bind(null, 0,null),
                                                                {target: {value: XlsxTaskType.singlePlusPersonalData}}
                                                            )}>
                                                            <div className={'task__back'}>XLSX</div>
                                                            <span
                                                                className={'task__name'}>Условия, личные исходники</span>
                                                        </div>
                                                        <div
                                                            className={`task ${currentTask?.options?.xlsxType === XlsxTaskType.PersonalData && '_active'}`}
                                                            onClick={onChangeTaskOption.bind(
                                                                null,
                                                                'xlsxType',
                                                                toggleEditMode.bind(null, 0,null),
                                                                {target: {value: XlsxTaskType.PersonalData}}
                                                            )}>
                                                            <div className={'task__back'}>XLSX</div>
                                                            <span
                                                                className={'task__name'}>Личные условия</span>
                                                        </div>
                                                        <div
                                                            className={`task ${currentTask?.options?.xlsxType === XlsxTaskType.sliceData && '_active'}`}
                                                            onClick={onChangeTaskOption.bind(
                                                                null,
                                                                'xlsxType',
                                                                toggleEditMode.bind(null, 0),
                                                            {target: {value: XlsxTaskType.sliceData}}
                                                            )}>
                                                            <div className={'task__back'}>XLSX</div>
                                                            <span
                                                                className={'task__name'}>Личные условия в 1 файле</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </>
                                        }
                                        {
                                            (currentTask?.type == TaskType.xlsx || currentTask?.type == TaskType.python) &&
                                            (currentTask?.options?.pythonType === PythonTaskType.singleTask ||
                                                currentTask?.options?.pythonType === PythonTaskType.singlePlusPersonalData ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.singleTask ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.singlePlusPersonalData) &&
                                            currentTask?.options?.mainFiles && editMode != 4 &&
                                            <div className={'_option'}>
                                                <div className={'task__file-select'}>
                                                    <span className={'task-item__name'}>Файлы основного условия: </span>
                                                    <button className={'button'} onClick={toggleEditMode.bind(null, 4,null)}>
                                                        Изменить
                                                    </button>
                                                </div>
                                                {
                                                    currentTask?.options?.mainFiles.length > 0 ?
                                                        currentTask?.options?.mainFiles.map((item: { file: { name: string } }, index: number) =>
                                                            <div key={index} className={'task__item _file'}>
                                                                <span
                                                                    className={'task-item__name'}>{item.file.name}</span>
                                                                <button className={'button'}
                                                                        onClick={deleteFileByName.bind(null, 'mainFiles', item.file.name)}>
                                                                    Удалить
                                                                </button>
                                                            </div>) :
                                                        <div className={'task-item__name'}>Файлов нет!</div>
                                                }
                                            </div>
                                        }
                                        {
                                            (currentTask?.type == TaskType.xlsx || currentTask?.type == TaskType.python) &&
                                            (currentTask?.options?.pythonType === PythonTaskType.singleTask ||
                                                currentTask?.options?.pythonType === PythonTaskType.singlePlusPersonalData ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.singleTask ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.singlePlusPersonalData) &&
                                            (editMode == 4 || !currentTask?.options?.mainFiles) &&
                                            <form className={'task__item _option'}
                                                  onSubmit={toggleEditMode.bind(null, 0,null)}>
                                                <span className={'task-item__name'}>Загрузите файлы условия: </span>
                                                <input type={'file'} multiple={true}
                                                       onChange={uploadFilesOption.bind(null, 'mainFiles')}/>
                                            </form>
                                        }
                                        {
                                            (currentTask?.type == TaskType.xlsx || currentTask?.type == TaskType.python) &&
                                            (currentTask?.options?.pythonType === PythonTaskType.PersonalData ||
                                                currentTask?.options?.pythonType === PythonTaskType.singlePlusPersonalData ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.PersonalData ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.singlePlusPersonalData) &&
                                            currentTask?.options?.personalFiles && editMode != 5 &&
                                            <div className={'_option'}>
                                                <div className={'task__file-select'}>
                                                    <span
                                                        className={'task-item__name'}>Файлы персонального условия: </span>
                                                    <button className={'button'} onClick={toggleEditMode.bind(null, 5,null)}>
                                                        Изменить
                                                    </button>
                                                </div>
                                                {
                                                    currentTask?.options?.personalFiles.length > 0 ?
                                                        currentTask?.options?.personalFiles.map((item: { file: { name: string } }, index: number) =>
                                                            <div key={index} className={'task__item _file'}>
                                                                <span
                                                                    className={'task-item__name'}>{item.file.name}</span>
                                                                <button className={'button'}
                                                                        onClick={deleteFileByName.bind(null, 'personalFiles', item.file.name)}>
                                                                    Удалить
                                                                </button>
                                                            </div>) :
                                                        <div className={'task-item__name'}>Файлов нет!</div>
                                                }
                                            </div>
                                        }
                                        {
                                            (currentTask?.type == TaskType.xlsx || currentTask?.type == TaskType.python) &&
                                            (currentTask?.options?.pythonType === PythonTaskType.PersonalData ||
                                                currentTask?.options?.pythonType === PythonTaskType.singlePlusPersonalData ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.PersonalData ||
                                                currentTask?.options?.xlsxType === XlsxTaskType.singlePlusPersonalData) &&
                                            (editMode == 5 || !currentTask?.options?.personalFiles) &&
                                            <form className={'task__item _option'}
                                                  onSubmit={toggleEditMode.bind(null, 0,null)}>
                                                <span className={'task-item__name'}>Загрузите файлы персонального условия: </span>
                                                <input type={'file'} multiple={true}
                                                       onChange={uploadFilesOption.bind(null, 'personalFiles')}/>
                                            </form>
                                        }
                                        {
                                            currentTask?.type == TaskType.xlsx &&
                                            (currentTask?.options?.xlsxType === XlsxTaskType.sliceData) &&
                                            currentTask?.options?.sliceFile && editMode != 5 &&
                                            <div className={'_option'}>
                                                <div className={'task__file-select'}>
                                                    <span
                                                        className={'task-item__name'}>Файл с условиями: </span>
                                                    <button className={'button'} onClick={toggleEditMode.bind(null, 6,null)}>
                                                        Изменить
                                                    </button>
                                                </div>
                                                {
                                                    currentTask?.options?.sliceFile.length > 0 ?
                                                        currentTask?.options?.sliceFile.map((item: { file: { name: string } }, index: number) =>
                                                            <div key={index} className={'task__item _file'}>
                                                                <span
                                                                    className={'task-item__name'}>{item.file.name}</span>
                                                                <button className={'button'}
                                                                        onClick={deleteFileByName.bind(null, 'sliceFile', item.file.name)}>
                                                                    Удалить
                                                                </button>
                                                            </div>) :
                                                        <div className={'task-item__name'}>Файлов нет!</div>
                                                }
                                            </div>
                                        }
                                        {
                                            currentTask?.type == TaskType.xlsx &&
                                            currentTask?.options?.xlsxType === XlsxTaskType.sliceData &&
                                            (editMode == 6 || !currentTask?.options?.sliceFile) &&
                                            <form className={'task__item _option'}
                                                  onSubmit={toggleEditMode.bind(null, 0,null)}>
                                                <span className={'task-item__name'}>Загрузите файл с условиями: </span>
                                                <input type={'file'}
                                                       onChange={uploadFilesOption.bind(null, 'sliceFile')}/>
                                            </form>
                                        }
                                        {
                                            currentTask?.type == TaskType.xlsx &&
                                            currentTask?.options?.xlsxType === XlsxTaskType.sliceData &&
                                                editMode != 8 &&
                                            <div className={'task__item'}>
                                                <span className={'task-item__name'}>Отступ сверху: </span>
                                                <span className={'task-item__value'}>{currentTask?.options?.sliceDataIndent || '0'}</span>
                                                <span className={'task-item__name'}>Высота задания: </span>
                                                <span className={'task-item__value'}>{currentTask?.options?.sliceDataHeight || '0'}</span>
                                                <span className={'task-item__name'}>Количество заданий: </span>
                                                <span className={'task-item__value'}>{currentTask?.options?.sliceDataCount || '0'}</span>
                                                <button className={'button'} onClick={toggleEditMode.bind(null, 8,null)}>
                                                    Изменить
                                                </button>
                                            </div>
                                        }
                                        {
                                            currentTask?.type == TaskType.xlsx &&
                                            currentTask?.options?.xlsxType === XlsxTaskType.sliceData &&
                                            (editMode == 8) &&
                                            <form className={'task__item'} onSubmit={toggleEditMode.bind(null, 0,null)}>
                                                <span className={'task-item__name'}>Отступ: </span>
                                                <input type={'number'} defaultValue={currentTask?.options?.sliceDataIndent}
                                                       onChange={onChangeTaskOptionWithSave.bind(null, 'sliceDataIndent',null)} required={true}/>
                                                <span className={'task-item__name'}>Высота: </span>
                                                <input type={'number'} defaultValue={currentTask?.options?.sliceDataHeight}
                                                       onChange={onChangeTaskOptionWithSave.bind(null, 'sliceDataHeight',null)} required={true}/>
                                                <span className={'task-item__name'}>Количество: </span>
                                                <input type={'number'} defaultValue={currentTask?.options?.sliceDataCount}
                                                       onChange={onChangeTaskOptionWithSave.bind(null, 'sliceDataCount',null)} required={true}/>
                                                <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                            </form>
                                        }
                                    </>
                                }
                                {
                                    currentTask?.type == TaskType.test &&
                                    <>
                                        {
                                            currentTask?.options?.test?.filter((_:any,i:number,arr:any[]) => arr.length - 1 > i)
                                                .map((test:{question:string,answer:string},testIndex:number) =>
                                                <div key={testIndex} className={'task__item'}>
                                                    <span className={'task-item__name'}>Вопрос: </span>
                                                    <span className={'task-item__value'}>{test.question || 'Введите вопрос!'}</span>
                                                    <span className={'task-item__name'}>Ответ: </span>
                                                    <span className={'task-item__value'}>{test.answer  || 'Введите ответ!'}</span>
                                                    <button className={'button'} onClick={deleteTestTaskByIndex.bind(null, testIndex)}>
                                                        Удалить
                                                    </button>
                                                </div>

                                            )
                                        }
                                        {
                                            editMode != 9 &&
                                            <div className={'task__item'}>
                                                <span className={'task-item__name'}/>
                                                <button className={'button'} onClick={toggleEditMode.bind(null, 9, null)}>
                                                    Добавить вопрос
                                                </button>
                                            </div>
                                        }
                                        {
                                            (editMode == 9) &&
                                            <form className={'task__item  _test'} onSubmit={toggleTestEditMode.bind(null, 0)}>
                                                <div className={'task-item__question'}>
                                                    <span className={'task-item__name'}>Введите вопрос: </span>
                                                    <input type={'text'}
                                                           onChange={addTestTask.bind(null, 'question')} required={true}/>
                                                </div>
                                                <div className={'task-item__answer'}>
                                                    <span className={'task-item__name'}>Введите ответ: </span>
                                                    <input type={'text'}
                                                           onChange={addTestTask.bind(null, 'answer')} required={true}/>

                                                </div>
                                                <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                            </form>
                                        }
                                    </>
                                }
                                {
                                    <div className={'task__item'}>
                                        <span className={'task-item__name'}/>
                                        {
                                            !isCompleted && <button className={'button'} onClick={saveTask}>
                                                Завершить редактирование
                                            </button>
                                        }
                                        {
                                            isCompleted && <button className={'button'} onClick={toggleEditMode.bind(null, 0,null)}>
                                                Редактировать задание
                                            </button>
                                        }
                                    </div>
                                }
                                {
                                    isCompleted &&
                                    <>
                                        <header className={'task__header'}>
                                            <h3 className={'task__h3'}>
                                                Публикация задания
                                            </h3>
                                        </header>
                                        {
                                            currentDeadlineDate && publicMode != 1 &&
                                            <div className={'task__item'}>
                                                <span className={'task-item__name'}>Дата дедлайна: </span>
                                                <span className={'task-item__value'}>{currentDeadlineDate}</span>
                                                <button className={'button'} onClick={togglePublicMode.bind(null, 1)}>
                                                    Изменить
                                                </button>
                                            </div>
                                        }
                                        {
                                            (publicMode == 1 || !currentDeadlineDate) &&
                                            <form className={'task__item'} onSubmit={togglePublicMode.bind(null, 0)}>
                                                <span className={'task-item__name'}>Введите дату дедлайна: </span>
                                                <input type={'date'}
                                                       onChange={setPublicState.bind(null, setCurrentDeadlineDate)}
                                                       required={true}/>
                                                <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                            </form>
                                        }
                                        {
                                            currentDeadlineTime && publicMode != 2 &&
                                            <div className={'task__item'}>
                                                <span className={'task-item__name'}>Время дедлайна: </span>
                                                <span className={'task-item__value'}>{currentDeadlineTime}</span>
                                                <button className={'button'} onClick={togglePublicMode.bind(null, 2)}>
                                                    Изменить
                                                </button>
                                            </div>
                                        }
                                        {
                                            (publicMode == 2 || !currentDeadlineTime) &&
                                            <form className={'task__item'} onSubmit={togglePublicMode.bind(null, 0)}>
                                                <span className={'task-item__name'}>Введите время дедлайна: </span>
                                                <input type={'time'}
                                                       onChange={setPublicState.bind(null, setCurrentDeadlineTime)}
                                                       required={true}/>
                                                <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                            </form>
                                        }
                                        {
                                            currentGroups && publicMode != 3 &&
                                            <div className={'task__item'}>
                                                <span className={'task-item__name'}>Группа: </span>
                                                <span
                                                    className={'task-item__value'}>{currentGroups?.map((item:any) =>
                                                    allGroups?.find((gr) =>
                                                        gr.id === item
                                                    )?.name + ', ')}</span>
                                                <button className={'button'}
                                                        onClick={togglePublicMode.bind(null, 3)}>
                                                    Изменить
                                                </button>
                                            </div>
                                        }
                                        {
                                            (publicMode == 3 || !currentGroups) &&
                                            <form className={'task__item'} onSubmit={togglePublicMode.bind(null, 0)}>
                                                <span className={'task-item__name'}>Выберите группу: </span>
                                                <select multiple={true} onChange={setPublicStateGroups.bind(null, setCurrentGroups)}>
                                                    <option value={'Выберите группу'}>Выберите группу</option>
                                                    {
                                                        allGroups.map((group, groupIndex) =>
                                                            <option value={group.id}
                                                                    key={groupIndex}>{group.name}</option>
                                                        )
                                                    }
                                                </select>
                                                <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                            </form>
                                        }
                                        {
                                            currentCoefficient && publicMode != 4 &&
                                            <div className={'task__item'}>
                                                <span className={'task-item__name'}>Коэффициент: </span>
                                                <span className={'task-item__value'}>{currentCoefficient}</span>
                                                <button className={'button'}
                                                        onClick={togglePublicMode.bind(null, 4)}>
                                                    Изменить
                                                </button>
                                            </div>
                                        }
                                        {
                                            (publicMode == 4 || !currentCoefficient) &&
                                            <form className={'task__item'} onSubmit={togglePublicMode.bind(null, 0)}>
                                                <span className={'task-item__name'}>Введите коэффициент: </span>
                                                <input type={'number'} min={0} max={2} step={0.1}
                                                       onChange={setPublicState.bind(null, setCurrentCoefficient)}
                                                       required={true}/>
                                                <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                            </form>
                                        }
                                        {
                                            (currentWorkType && publicMode != 5) &&
                                            <div className={'task__item'}>
                                                <span className={'task-item__name'}>Тип работы: </span>
                                                <span
                                                    className={'task-item__value'}>{workTypes.find(item => item.type == currentWorkType)?.value || ''}</span>
                                                <button className={'button'}
                                                        onClick={togglePublicMode.bind(null, 5)}>
                                                    Изменить
                                                </button>
                                            </div>
                                        }
                                        {
                                            (publicMode == 5 || !currentWorkType) &&
                                            <form className={'task__item'} onSubmit={togglePublicMode.bind(null, 0)}>
                                                <span className={'task-item__name'}>Введите тип работы: </span>
                                                <select onChange={setPublicState.bind(null, setCurrentWorkType)}>
                                                    {
                                                        workTypes.map((work, workIndex) =>
                                                            <option value={work.type}
                                                                    key={workIndex}>{work.value}</option>
                                                        )
                                                    }
                                                </select>
                                                <input className={'button'} type={'submit'} value={'Сохранить'}/>
                                            </form>
                                        }
                                        <div className={'task__item'}>
                                            <span className={'task-item__name'}/>
                                            {
                                                <button className={'button'}
                                                        onClick={publish}>
                                                    ОПУБЛИКОВАТЬ
                                                </button>
                                            }
                                        </div>

                                    </>
                                }
                            </div>
                        </div>
                    }
                    {
                        editMode == -1 &&
                        <div className={'task__empty'}>
                            <ArrowLeftIcon className={'icon'}/>
                            <div>Выберите задание!</div>
                        </div>
                    }
                </section>
            </article>
        </ContentWrapper>
    )
}

const TaskCreator = memo(taskCreatorFC)

export default TaskCreator