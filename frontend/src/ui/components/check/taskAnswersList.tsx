import React, {memo, useEffect, useState} from "react";
import {Link, useRouteMatch} from "react-router-dom";
import {useApiContext} from "../../utils/contexts/api-context";

type Props = {
    id:string
}

const taskAnswersListFC:React.FC<Props> = ({id}:Props) => {
    let {path} = useRouteMatch();

    path = path.replace(/\/$/, '');

    type Solution = {
        id: string,
        student_id: string,
        name: string,
        date: string,
        mark?:boolean,
    }

    const [solutions, setSolutions] = useState<Solution[]>([])

    const api = useApiContext()

    const fetchData = async () => {
        const data: {
            solutions: Solution[]
        } = {
            solutions: []
        }

        await api?.get('solvedtasks').then((res) => {
            data.solutions = res.filter((item: any) => item.task_id === id)

            console.log(res)
        })

        return data
    }

    const update =() => {
        fetchData().then((data) => {
            setSolutions(data.solutions)
        })
    }

    useEffect(() => {
        update()
    },[])

    return (
        <>
            {
                solutions &&
                solutions.filter(item => !item.mark).map((solution, solutionIndex) =>
                    <Link key={solutionIndex} to={path + '/' + solution.id} className={'task-link'}>
                        <div className={'link-name'}>
                            {
                                solution.name
                            }
                        </div>
                        <div className={'link-date'}>
                            {
                                solution.date
                            }
                        </div>
                    </Link>
                )
            }
            {
                solutions &&
                solutions.filter(item => item.mark).map((solution, solutionIndex) =>
                    <Link key={solutionIndex} to={path + '/' + solution.id} className={'task-link'}>
                        <div className={'link-name'}>
                            {
                                solution.name
                            }
                        </div>
                        <div>Проверено</div>
                        <div className={'link-date'}>
                            {
                                solution.date
                            }
                        </div>
                    </Link>
                )
            }
            {
                !solutions &&
                <div className={'empty'}>
                    Ничего нет!
                </div>
            }

        </>
    )
}

const TaskAnswersList = memo(taskAnswersListFC)

export default TaskAnswersList;