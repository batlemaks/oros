import React, {memo, useEffect, useState} from "react";

import './check.scss'
import ContentWrapper from "../content-wrapper/content-wrapper";
import {UpdateIcon} from "@modulz/radix-icons";
import {NavLink, useRouteMatch} from "react-router-dom";
import RouteRenderer from "../../utils/route-renderer";
import {useStore} from "react-redux";
import {StudentTaskType, TaskType} from "../task-creator/task-creator";
import Task from "../tasks/task";
import TaskForCheck from "./taskForCheck";
import {useApiContext} from "../../utils/contexts/api-context";

type StudentTask = StudentTaskType & {
    active?:boolean,
    schema_id: string,
    deadline_id: string
}

const checkFC:React.FC = () => {
    document.title = "ИО - Проверка заданий"

    let {path, url} = useRouteMatch();

    path = path.replace(/\/$/, '');
    url = url.replace(/\/$/, '');

    const [tasks, setTasks] = useState<StudentTask[] | undefined>(undefined)
    const api = useApiContext()

    const fetchData = async () => {
        const data: {
            tasks: StudentTask[]
        } = {
            tasks: []
        }

        await api?.get('tasks').then((res) => {
            data.tasks = res
        })

        await api?.get('taskschemas').then((res) => {
            data.tasks = data.tasks.map((item) => {
                return {
                    ...res.find((schema: any) => schema.id === item.schema_id),
                    ...item,
                    id: item.id
                }
            })
        })

        await api?.get('deadlines').then((res) => {
            data.tasks = data.tasks.map((item) => {
                return {
                    ...item,
                    active: Date.parse(res.find((deadline: any) => deadline.id === item.deadline_id)?.time) > Date.now()
                }
            })
        })

        return data
    }

    const update = () => {
        fetchData().then((data) => {
            setTasks(data.tasks)
        })
    }

    useEffect(() => {
        update()
    }, [])

    const TaskByID = (id:string) => <TaskForCheck id={id}/>

    return (
        <ContentWrapper minWidth={1300} maxWidth={1300} maxHeight={900}>
            <article className={'student-tasks'}>
                <header className={'student-tasks__header'}>
                    <div className={'student-tasks__info'}>
                        <h2 className={'student-tasks__h2'}>
                            Проверка заданий
                        </h2>
                    </div>
                    <div className={'student-tasks__buttons'}>
                        <button onClick={update} className={'button'}>
                            <span className={'icon'}>
                                <UpdateIcon/>
                            </span>
                        </button>
                    </div>
                </header>
                <section className={'student-tasks__area'}>
                    <div className={'task-list'}>
                        <div className={'task-list__tasks'}>
                            <header className={'task-list__header _inner'}>
                                <h3 className={'task-list__h3'}>Активные</h3>
                            </header>
                            {
                                tasks?.filter(task=>task.active).map((task, taskIndex) =>
                                    <NavLink to={path + '/' + task.id} key={taskIndex} className={'task'} activeClassName={'_active'}>
                                        <div className={'task__back'}>{task.type}</div>
                                        <span className={'task__name'}>{task.name}</span>
                                    </NavLink>
                                )
                            }
                            <header className={'task-list__header _inner'}>
                                <h3 className={'task-list__h3'}>Неактивные</h3>
                            </header>
                            {
                                tasks?.filter(task=>!task.active).map((task, taskIndex) =>
                                    <NavLink to={path + '/' + task.id} key={taskIndex} className={'task'} activeClassName={'_active'}>
                                        <div className={'task__back'}>{task.type}</div>
                                        <span className={'task__name'}>{task.name}</span>
                                    </NavLink>
                                )
                            }
                        </div>
                    </div>
                    <RouteRenderer routes={tasks?.map(item => {
                        return {
                            ...item,
                            path:item.id,
                            component: TaskByID.bind(null,item.id)
                        }
                    })} path={url+'/'} />
                </section>
            </article>
        </ContentWrapper>
    )
}

const Check = memo(checkFC)

export default Check;