import React, {memo, useState} from "react";
import {UserRole} from "../../utils/constants/permission-constants";
import {Link, useLocation} from "react-router-dom";
import {useStore} from "react-redux";
import {DoubleArrowLeftIcon, DoubleArrowRightIcon} from "@modulz/radix-icons";
import {TabsType} from "../../routes/home-page/home-page";

import './bars.scss'

type Props = {
    permissions: UserRole[],
    routes: TabsType[],
    path:string,
    setIsCollapsed: (_:boolean) => void,
    isCollapsed:boolean
}

const leftBarFC:React.FC<Props> = ({permissions, routes, path, isCollapsed, setIsCollapsed}:Props) => {
    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
    })

    const location = useLocation().pathname.split('/')
        .filter(item => item.indexOf('?') === -1)
        .filter(item => item !== '').reverse()[0];

    if (!permissions.includes(userRole))
        return null

    return (
        <div className={`left-bar ${isCollapsed && '_collapse'}`}>
                <div className={'left-bar__links'}>
                {
                    routes.map((item, index) => {
                        return <Link title={isCollapsed&&item.title||''} key={index} className={`link ${location === item.path ? '_active' : ''}`}
                                     to={path + item.path}><span
                            className={'link-icon'}>{item.icon}</span>{!isCollapsed && item.title}</Link>
                    })
                }
            </div>
            <span onClick={() => setIsCollapsed(!isCollapsed)} className={'left-bar__collapse'}>
                <span className={'link-icon'}>{!isCollapsed ? <DoubleArrowLeftIcon/> :
                    <DoubleArrowRightIcon/>}</span>{!isCollapsed && 'Скрыть названия'}
            </span>
        </div>
    )
}

const LeftBar = memo(leftBarFC)

export default LeftBar