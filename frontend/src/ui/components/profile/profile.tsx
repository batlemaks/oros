import React, {memo, useEffect, useState} from "react";

import './profile.scss'
import ContentWrapper from "../content-wrapper/content-wrapper";
import {Cross2Icon, GearIcon} from "@modulz/radix-icons";
import {useStore} from "react-redux";
import {Redirect, useLocation, useParams} from "react-router-dom";
import {TAGS} from "../news/news";
import {useApiContext} from "../../utils/contexts/api-context";
import Question from "../question/question";
import {useMsal} from "@azure/msal-react";
import {loginRequest} from "../../../../auth/authConfig";
import {callMsGraph} from "../../../../auth/graph";
import {UserRole} from "../../utils/constants/permission-constants";
import {useMessageContext} from "../../routes/home-page/home-page";

export const getSearchParamFromLocation = (searchParam: string, {search}: {search: string}) => {
    if (!search) {
        return '';
    }

    const searchParts = search.split('&');
    if (!searchParts.length) {
        return '';
    }

    const searchPartsWithId = searchParts.filter((part) => part.includes(`${searchParam}=`));
    if (!searchPartsWithId.length) {
        return '';
    }

    return searchPartsWithId[0].replace('?', '').replace(`${searchParam}=`, '');
};

type Data = {
    attrName:string,
    attRealName:string,
    data:string
}

type AppData = {
    attrName:string,
    attRealName:string,
    editName:string,
    variants:any[],
    default:any[],
    isMultiple?: boolean,
}

type ProfileData = {
    id: string,
    profileData: Data[]
}

const APP_DATA: AppData[] = [
    {
        attRealName:'Приходят уведомления с тегами',
        attrName:'notifications',
        default: TAGS.map(item => item.msg).filter(item => item !== 'Выберите метки'),
        variants: TAGS.map(item => item.msg).filter(item => item !== 'Выберите метки'),
        isMultiple:true,
        editName:'Получать уведомления с тегами'
    },
    {
        attRealName:'Язык',
        attrName:'lang',
        default:['RU'],
        variants:['RU'],
        editName:'Выберите язык'
    },
]

const profileFC:React.FC = () => {
    document.title = "ИО - Профиль"

    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)
    const [userID, setUserID] = useState(store.getState().user.userData.id)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
        setUserID(store.getState().user.userData.id)
    })

    const {setSuccess, setError: setError1} = useMessageContext() || {}

    const admin = [3, 4].includes(userRole)

    const [adminEditMode, setAdminEditMode] = useState(false)

    let {id} = useParams<{ id: string }>();

    const ownProfile = id === 'self'

    id = id === 'self' ? userID : id

    const onAdminEditMode = () => {
        setAdminEditMode(!adminEditMode)
    }

    const api = useApiContext()

    const fetchData = async () => {
        const test: ProfileData = {
            id:'',
            profileData: []
        }

        await api?.get(`users/${id}`).then((data) => {
            const profileDataNames: {[k in string]: string} = {
                'name': 'ФИО',
                'role': 'Роль',
                'email': 'Email',
                'date': 'Дата регистрации'
            }

            test.id = id
            test.profileData = Object.entries(data).filter((item) =>
                Object.keys(profileDataNames).includes(item[0])
            ).map((item:any) => {
                return {
                    attrName: item[0],
                    attRealName: profileDataNames[item[0]],
                    data: item[1]
                }
            }).reverse()
        })

        return test
    }

    const [profileData, setProfileData] = useState<Data[]>()
    const [appData, setAppData] = useState<AppData[]>([])
    const [error, setError] = useState(false)

    const getAppData = (): AppData[] => {
        return APP_DATA.map((item): AppData => {
                const data = {...item}

                const cook = api?.getCookies(item.attrName)?.split(',')

                data.default = cook &&
                cook.filter(variant => item.variants.includes(variant)).length > 0 ?
                    cook.filter((val, i, ar) => ar.indexOf(val) === i) :
                    item.default

                return data
            }
        )
    }

    const [listOfTags, setListOfTags] = useState<string[]>([])
    const [lang, setLang] = useState<string[]>([])

    const deleteTag = (index: number) => {
        const tags = listOfTags.filter((_, i) => i !== index)

        setListOfTags(tags)
    }

    const changeTabList = (val: { target: { value: string } }) => {
        const value = val.target.value

        if (value === TAGS[0].msg)
            return null

        let tags = listOfTags
        tags.push(value)
        tags = tags.filter((val, i, ar) => ar.indexOf(val) === i);

        setListOfTags(tags)
    }

    let langEd: string[]

    const changeLang = (val: { target: { value: string } }) => {
        const value = val.target.value

        langEd = [value]
    }

    const onCancel = () => {
        setAdminEditMode(false)
        setListOfTags([])
        setLang([])
    }

    const [profileForDeletion, setProfileForDeletion] = useState<string | undefined>(undefined)

    const confirmDeleteProfile = (id: string) => {
        setProfileForDeletion(id)
    }

    const deleteProfile = () => {
        api?.delete(`users/${id}`).then(() => {
            setProfileForDeletion(undefined)
        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError1 && setError1()
        })
    }

    const cancelDeleteProfile = () => {
        setProfileForDeletion(undefined)
    }

    const onSave = () => {
        api?.setCookies(APP_DATA[0].attrName, listOfTags)
        api?.setCookies(APP_DATA[1].attrName, langEd)

        setAdminEditMode(false)
    }

    useEffect(() => {
        fetchData().then((data) => {
            if (data)
                setProfileData(data.profileData)
            else
                setError(true)

            if (ownProfile) {
                const ownAppData = getAppData()
                setListOfTags(ownAppData[0].default)
                setLang(ownAppData[1].default)

                setAppData(ownAppData)
            }
        })


    }, [adminEditMode, userID])

    if (error) {
        return <Redirect to={'/error'}/>
    }

    return (
        <ContentWrapper minWidth={900}>
            <article className={'profile'}>
                <header className={'profile-header'}>
                    <div className={'profile-header__info'}>
                        <h2 className={'profile-header__h2'}>Профиль</h2>
                    </div>
                    {
                        <div className={'profile-header__buttons'}>
                            {
                                adminEditMode && admin &&
                                <button onClick={confirmDeleteProfile.bind(null, id)} className={'button _red'}>
                                    Удалить пользователя
                                </button>
                            }
                            <button className={'button'} onClick={onAdminEditMode}>
                                <span className={'icon'}><GearIcon/></span>
                            </button>
                        </div>
                    }
                </header>
                <section className={'profile-data'}>
                    <div className={'profile-data__data'}>
                        <div className={'profile-data__user-data'}>
                            <h3 className={'profile-data__h3'}>
                                Данные о пользователе:
                            </h3>
                            <div className={'profile-data__list'}>
                                {
                                    profileData?.map((item, index) =>
                                        <div key={index} className={'profile-data__item'}>
                                            <span className={'profile-data__item-name'}>
                                                {item.attRealName + ': '}
                                            </span>
                                            <span className={'profile-data__item-data'}>
                                                {item.data}
                                            </span>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                        {
                            ownProfile &&
                            <div className={'profile-data__user-settings'}>
                                <h3 className={'profile-data__h3'}>
                                    Настройки приложения:
                                </h3>
                                <div className={'profile-data__list'}>
                                    {
                                        appData?.map((item, index) =>
                                            <div key={index} className={'profile-data__item'}>
                                                <span className={'profile-data__item-name'}>
                                                    {
                                                        !adminEditMode ?
                                                            item.attRealName:
                                                            item.editName  + ': '
                                                    }
                                                </span>
                                                {
                                                    !adminEditMode ?
                                                        <span className={'profile-data__edit-tags'}>
                                                        {
                                                            index === 0 ?
                                                                item.default.map((item, index) => {
                                                                    const color = TAGS.filter(tag => item === tag.msg)[0].color || 'grey'

                                                                    return <div key={index}
                                                                                className={`tag _${color}`}>{item}</div>
                                                                }) :
                                                                item.default.join(', ')
                                                        }
                                                        </span> :
                                                        index === 0 ?
                                                            <>
                                                                <select className={'select'} onChange={changeTabList}>
                                                                    {TAGS.map(item => <option key={item.msg}
                                                                                              value={item.msg}>{item.msg}</option>)}
                                                                </select>
                                                                <div className={'profile-data__edit-tags'}>
                                                                    {
                                                                        listOfTags.map((item, index) => {
                                                                            const color = TAGS.filter(tag => item === tag.msg)[0].color || 'grey'
                                                                            const deleteTagByIndex = deleteTag.bind(null, index)

                                                                            return <div key={index}
                                                                                        className={`tag _${color}`}>{item}<span
                                                                                className={'delete-tag-icon'}
                                                                                onClick={deleteTagByIndex}><Cross2Icon/></span>
                                                                            </div>
                                                                        })
                                                                    }
                                                                </div>
                                                            </> :
                                                            <select multiple={item.isMultiple} onChange={changeLang}
                                                                    className={'profile-data__item-data'}>
                                                                <option key={index} value={lang}>{lang}</option>
                                                                {
                                                                    item.variants.filter(item => item != lang).map((item, index) =>
                                                                        <option key={index}
                                                                                value={item}>{item}</option>)
                                                                }
                                                            </select>
                                                }
                                            </div>
                                        )
                                    }
                                </div>
                            </div>
                        }
                    </div>
                    {
                        adminEditMode && ownProfile &&
                        <div className={'profile-data__edit-mode'}>
                            <div className={'profile-data__buttons'}>
                                <button className={'button _red'} onClick={onCancel}>
                                    Отменить изменения
                                </button>
                                <button className={'button _green'} onClick={onSave}>
                                    Сохранить изменения
                                </button>
                            </div>
                        </div>
                    }
                </section>
            </article>
            <Question trigger={profileForDeletion} message={'Уверены, что хотите удалить Профиль?'}
                      buttonMessage={'Продолжить'} action1={cancelDeleteProfile} action2={deleteProfile}/>
        </ContentWrapper>
    )
}

const Profile = memo(profileFC)

export default Profile;
