import React, {memo, useEffect, useState} from "react";
import ContentWrapper from "../content-wrapper/content-wrapper";
import {UpdateIcon} from "@modulz/radix-icons";
import {useStore} from "react-redux";

import './users.scss'
import {useApiContext} from "../../utils/contexts/api-context";
import {useMessageContext} from "../../routes/home-page/home-page";

type Request = {
    name: string,
    email: string,
    id: string
    reqID: string
}

const usersFC:React.FC = () => {
    document.title = "ИО - Пользователи"

    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
    })

    const admin = [3, 4].includes(userRole)

    const [viewMode, setViewMode] = useState(false)

    const changeViewMode = () => {
        setViewMode(!viewMode)
    }

    const api = useApiContext()

    const [requests, setRequests] = useState<Request[]>()
    const fetchData = async () => {
        const data: {
            requests: Request[]
        } = {requests: []}

        await api?.get('requests').then((res) => {
            console.log(res)

            data.requests = res.filter((item:any) => item.type === 'REGISTER_USER').map((item:any) => {
                return {
                    ...JSON.parse(item.source),
                    reqID: item.id
                }

            })
        })

        return data
    }

    const updateRequests = () => {
        fetchData().then((data) => {
            setRequests(data.requests)
        })
    }

    useEffect(() => {
        updateRequests()
    }, [])

    const {setSuccess, setError} = useMessageContext() || {}

    const submit = (reqID: string) => {
        api?.post(`requests/submit/${reqID}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateRequests()
        })
    }

    const cancel = (reqID: string) => {
        api?.delete(`requests/${reqID}`).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            updateRequests()
        })
    }

    return (
        <ContentWrapper minWidth={900} maxWidth={1200}>
            <article className={'users'}>
                <header className={'users-header'}>
                    <div className={'users__info'}>
                        <h2 className={'users__h2'}>
                            Управление пользователями
                        </h2>
                    </div>
                    <div className={'users__buttons'}>
                        {
                            admin &&
                            <button className={'button'} onClick={changeViewMode}>
                                Добавить студентов
                            </button>
                        }
                        <button className={'button'} onClick={updateRequests}><span
                            className={'icon'}><UpdateIcon/></span>
                        </button>
                    </div>
                </header>
                {
                    viewMode &&
                    <section className={'users__import'}>
                        <div className={'users-import-header'}>
                            <h3 className={'users-import__h3'}>Загрузка файла:</h3>
                            <button className={'button'}>
                                Загрузить
                            </button>
                        </div>
                        <div className={'users-import__content'}>
                            <div className={'users-import__info'}>
                                <div className={'users-import__text'}>
                                    Выберите файл формата <b className={'bold'}>.xlsx</b> подходящей структуры:
                                </div>
                                <div className={'users-import__text'}>
                                    <input type={'file'}/>
                                </div>
                            </div>
                            <div className={'users-import-sample'}>
                                <table>
                                    <thead>
                                    <tr className={'users-import-sample__header'}>
                                        <th>ФИО студента</th>
                                        <th>Почта студента</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Студент №1</td>
                                        <td>student1Email@edu.hse.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Студент №2</td>
                                        <td>student2Email@edu.hse.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Студент №3</td>
                                        <td>student3Email@edu.hse.ru</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                }
                <section className={'users__requests'}>
                    <div className={'users-requests-header'}>
                        <h3 className={'users-requests__h3'}>Заявки на вход в систему:</h3>
                    </div>
                    <div>
                        {
                            requests?.map((request, requestId) =>
                                <div key={requestId} className={'request'}>
                                    <div className={'request__content'}>
                                        <span className={'request__name'}>{request.name}</span>
                                        <span className={'request__email'}>({request.email})</span></div>
                                    <div className={'request__buttons'}>
                                        <button className={'button'} onClick={submit.bind(null, request.reqID)}>
                                            Принять
                                        </button>
                                        <button className={'button'} onClick={cancel.bind(null, request.reqID)}>
                                            Отклонить
                                        </button>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </section>
            </article>
        </ContentWrapper>
    )
}

const Users = memo(usersFC)

export default Users