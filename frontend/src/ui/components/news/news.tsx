import React, {memo, useContext, useEffect, useState} from "react";

import './news.scss'
import ContentWrapper from "../content-wrapper/content-wrapper";
import {useStore} from "react-redux";
import {Cross2Icon, MixerVerticalIcon, PaperPlaneIcon, Pencil2Icon, UpdateIcon} from "@modulz/radix-icons";
import Question from "../question/question";
import {useMessageContext} from "../../routes/home-page/home-page";
import {useApiContext} from "../../utils/contexts/api-context";

export const TAGS = [
    {
        msg:'Выберите метки',
        color:'black'
    },
    {
        msg:'Важно',
        color:'red'
    },
    {
        msg:'Экзамен',
        color:'blue'
    },
    {
        msg:'Домашняя работа',
        color:'green'
    },
    {
        msg:'Прочее',
        color:'gray'
    },
]

type Post = {
    author:string,
    authorID:string,
    postID:string,
    msg:string,
    date:string,
    tags:string[]
}

const newsFC:React.FC = () => {
    document.title = "ИО - Новости"
    const store = useStore()
    const [userRole, setUserRole] = useState(store.getState().user.userData.role)

    store.subscribe(() => {
        setUserRole(store.getState().user.userData.role)
    })

    const writer = [2, 3, 4].includes(userRole)

    const [filterOpen, setFilterOpen] = useState(false)
    const [writeMode, setWriteMode] = useState(false)
    const [listOfTags, setListOfTags] = useState<string[]>([])
    const [msg, setMsg] = useState('')
    const [authors, setAuthors] = useState(['Выбрать автора'])
    const [posts, setPosts] = useState<Post[]>([])
    const [allPosts, setAllPosts] = useState<Post[]>([])

    const {setSuccess, setError} = useMessageContext() || {}

    const onClickWrite = () => setWriteMode(!writeMode)

    const changeTabList = (val: { target: { value: string } }) => {
        const value = val.target.value

        if (value === TAGS[0].msg)
            return null

        let tags = listOfTags
        tags.push(value)
        tags = tags.filter((val, i, ar) => ar.indexOf(val) === i);

        setListOfTags(tags)
    }

    const deleteTag = (index: number) => {
        const tags = listOfTags.filter((_, i) => i !== index)

        setListOfTags(tags)
    }

    const sendMsg = () => {
        api?.post('news',{
            value: msg,
            tags: JSON.stringify(listOfTags)
        }
        ).then(() => {
            setSuccess && setSuccess()
        }).catch((e) => {
            setError && setError()
        }).then(() => {
            setListOfTags([])
            setWriteMode(false)
            setMsg('')
            updateAllPosts()
        })
    }

    const api = useApiContext()

    const fetchData = async () => {
        let test: Post[] = []

        await api?.get('news')
            .then((res) => {
                test = res.map((item: {value: string, date: string, tags:string, id:string}) => {
                    return {
                        msg: item.value,
                        date: item.date,
                        tags: JSON.parse(item.tags) || [],
                        author:'Преподаватель',
                        authorID:'001',
                        postID: item.id,
                    }
                }).reverse()

                console.log(test)
            })

        return test
    }

    const updateAllPosts = () => {
        fetchData().then((res) => {
            const dataAuthors = authors
            dataAuthors.push(...res.map(item => item.author).filter((val, i, ar) => ar.indexOf(val) === i))

            setAuthors(dataAuthors)
            setAllPosts(res)
        }).then(() => {
            setFilterOpen(false)
            //updatePosts()
        })
    }

    const [postForDeletion, setPostForDeletion] = useState<Post | undefined>(undefined)

    const confirmDeletePost = (post: Post) => {
        setPostForDeletion(post)
    }

    const deletePost = () => {
        api?.delete(`news/${postForDeletion?.postID}`).then(() => {
            setPostForDeletion(undefined)
            updateAllPosts()
        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        })
    }

    const cancelDeletePost = () => {
        setPostForDeletion(undefined)
    }

    const [editPostMode, setEditPostMode] = useState(false)
    const [editSaved, setEditSaved] = useState<any | undefined>(undefined)

    const changeMsg = (val: { target: { value: string } }) => {
        if (editPostMode) {
            setMsg(editSaved.msg || '')
            setEditSaved({...editSaved, msg:val.target.value})
        } else
            setMsg(val.target.value)
    }

    const editPost = (post: Post) => {
        setEditSaved(post)
        setListOfTags([...post.tags])
        setWriteMode(true)
        setEditPostMode(true)
    }

    const saveMsg = () => {
        api?.put(`news/${editSaved.postID}`,{
            value: editSaved.msg,
            tags: JSON.stringify(listOfTags)
        }).then(() => {
            setSuccess && setSuccess()
        }).catch(() => {
            setError && setError()
        }).then(() => {
            setListOfTags([])
            setWriteMode(false)
            setEditPostMode(false)
            setMsg('')
            setEditSaved('')
            updateAllPosts()
        })
    }

    useEffect(() => {
        fetchData().then((res) => {
            const dataAuthors = authors
            dataAuthors.push(...res.map(item => item.author).filter((val, i, ar) => ar.indexOf(val) === i))

            setAuthors(dataAuthors)
            setAllPosts(res)
        })
    }, [])

    const updatePosts = (val?: { target: { value: string, id: string } }) => {
        let currentPosts = allPosts //TODO ПРОБЛЕМА
        const value = val?.target.value || ''

        if (val?.target.id === 'tags') {
            currentPosts = currentPosts.filter(item => item.tags.includes(value))//TODO ПРОБЛЕМА
        }

        if (val?.target.id === 'authors') {
            currentPosts = currentPosts.filter(item => item.author.includes(value))//TODO ПРОБЛЕМА
        }

        setPosts(currentPosts)
        console.log(currentPosts)
    }

    const onFilterClick = () => {
        filterOpen && updatePosts()
        setFilterOpen(!filterOpen)
    }

    useEffect(() => {
        updatePosts()
        //
    }, [allPosts])

    return (
        <ContentWrapper  minWidth={950} maxWidth={950}>
            <div className={'news'}>
                <div className={'news-header'}>
                    <h2 className={'news-header__h2'}>События курса</h2>
                    <div className={'news-header__buttons'}>
                        {writer && <button onClick={onClickWrite}
                                           className={`button ${writeMode ? '_active' : ''}`}>Написать</button>}
                        {filterOpen &&
                        <>
                            <select className={'select'} id={'authors'} onChange={updatePosts}>
                                {authors.map(item => <option key={item} value={item}>{item}</option>)}
                            </select>
                            <select className={'select'} id={'tags'} onChange={updatePosts}>
                                {TAGS.map(item => <option key={item.msg} value={item.msg}>{item.msg}</option>)}
                            </select>
                        </>}
                        <button onClick={onFilterClick} className={`button ${filterOpen ? '_active' : ''}`}><span
                            className={'icon'}><MixerVerticalIcon/></span></button>
                        <button onClick={updateAllPosts} className={'button'}><span
                            className={'icon'}><UpdateIcon/></span>
                        </button>
                    </div>

                </div>
                {
                    writeMode &&
                    <div className={'news-writer'}>
                        <textarea placeholder={'Введите текст сообщения...'} value={editSaved ? editSaved.msg : msg}
                                  onChange={changeMsg}/>
                        <div className={'news-writer__controls'}>
                            <div className={'news-writer__buttons'}>
                                <select className={'select'} onChange={changeTabList}>
                                    {TAGS.map(item => <option key={item.msg} value={item.msg}>{item.msg}</option>)}
                                </select>
                                {
                                    listOfTags.map((item, index) => {
                                        const color = TAGS.filter(tag => item === tag.msg)[0].color || 'grey'
                                        const deleteTagByIndex = deleteTag.bind(null, index)

                                        return <div key={index} className={`tag _${color}`}>{item}<span
                                            className={'delete-tag-icon'}
                                            onClick={deleteTagByIndex}><Cross2Icon/></span></div>
                                    })
                                }
                            </div>
                            {editPostMode ? <button className={'button'} onClick={saveMsg}>Изменить <span
                                    className={'icon'}><PaperPlaneIcon/></span>
                                </button> :
                                <button className={'button'} onClick={sendMsg}>Отправить <span
                                    className={'icon'}><PaperPlaneIcon/></span>
                                </button>}
                        </div>
                    </div>
                }
                {
                    posts.map((item, index) =>
                        <div key={index}
                             className={`news-post _${TAGS.filter(tag => (item.tags[0] || '') === tag.msg)[0]?.color || ''}`}>
                            <div className={'news-post__header'}>
                                <span className={'news-post__author'}>{item.author}</span>
                                {writer &&
                                <div className={'news-post__buttons'}>
                                    <div onClick={editPost.bind(null, item)} className={'button'}><span
                                        className={'icon'}><Pencil2Icon/></span>
                                    </div>
                                    <div onClick={confirmDeletePost.bind(null, item)} className={'button'}><span
                                        className={'icon'}><Cross2Icon/></span>
                                    </div>
                                </div>
                                }
                            </div>
                            <pre className={'news-post__message'}>
                                {item.msg}
                            </pre>
                            <div className={'news-post__info'}>
                                <div className={'news-post__tags'}>
                                    {item.tags.map((item, index) => {
                                        const color = TAGS.filter(tag => item === tag.msg)[0].color || 'grey'

                                        return <div key={index} className={`tag _${color}`}>{item}</div>
                                    })}
                                </div>
                                <div className={'news-post__date'}>
                                    {item.date}
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
            <Question trigger={postForDeletion} message={'Уверены, что хотите удалить запись?'}
                      buttonMessage={'Продолжить'} action1={cancelDeletePost} action2={deletePost}/>
        </ContentWrapper>
    )
}

const News = memo(newsFC)

export default News;