import React, {memo} from "react";

import './question.scss'

type Props = {
    trigger: any,
    message: string,
    buttonMessage: string,
    action1: () => void,
    action2: () => void,
    justMessage?:boolean
}

const questionFC:React.FC<Props> = ({trigger,message,buttonMessage,action1,action2, justMessage}:Props) => {
    return (
        trigger ?
            <div className={'question-area'} onClick={action1}>
                <div className={'question'}>
                    <span className={'question__message'}>{message}</span>
                    <div className={'question__buttons'}>
                        {!justMessage&&<button className={'button'} onClick={action1}>Отменить</button>}
                        <button className={'button'} onClick={action2}>{buttonMessage}</button>
                    </div>
                </div>
            </div> : null
    )
}

const Question = memo(questionFC)

export default Question;