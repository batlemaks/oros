import React, {memo, useEffect, useState} from "react";
import {UpdateIcon} from "@modulz/radix-icons";
import ContentWrapper from "../content-wrapper/content-wrapper";
import {DATE, TIME} from "../groups/groups";

import './schedule.scss'
import {useApiContext} from "../../utils/contexts/api-context";

const scheduleFC : React.FC = () => {
    document.title = "ИО - Расписание"

    const api = useApiContext()

    const fetchGroups = async () => {
        const test = {groups: []}

        await api?.get('groups').then((res) => {
            test.groups = res
        })

        return test
    }

    type Event = {
        event: {
            date:string,
            time:string,
        },
        value: string,
        type:string
    }

    const [events, setEvents] = useState<Event[]>([])

    const updateGroups = () => {
        fetchGroups().then((data) => {
            const sh:Event[] = [];

            data.groups.map((item: any) => {
                return {
                    name: item.name,
                    lecture: item.events && JSON.parse(item.events).lecture,
                    seminar: item.events && JSON.parse(item.events).seminar,
                    students: item.students || [],
                    id: item.id
                }
            }).forEach((element) => {
                if (element.lecture && element.seminar){
                    sh.push({event: element.lecture, value:element.name, type:'Лекция'})
                    sh.push({event: element.seminar, value:element.name, type:'Семинар'})
                }
            })

            setEvents(sh)
        })
    }

    useEffect(() => {
        updateGroups()
    },[])

    return (
        <ContentWrapper minWidth={950} maxWidth={950}>
            <article className={'schedule'}>
                <header className={'schedule-header'}>
                    <div className={'schedule__info'}>
                        <h2 className={'schedule__h2'}>
                            Расписание
                        </h2>
                    </div>
                    <div className={'schedule__buttons'}>
                        <button className={'button'} onClick={updateGroups}><span
                            className={'icon'}><UpdateIcon/></span>
                        </button>
                    </div>
                </header>
                <section className={'schedule__content'}>
                    <table className={'schedule__table'}>
                        <thead className={'schedule__header'}>
                            <tr>
                                <th/>
                                {
                                    DATE.map((item, index) =>
                                        <th key={index} className={`cell`} >{item}</th>
                                    )
                                }
                            </tr>
                        </thead>
                        <tbody className={'schedule__content'}>
                        {
                            TIME.map((time,index)=>
                            <tr key={index} className={'schedule-row'}>
                                <th className={'schedule-row-header'}>{time}</th>
                                {
                                    DATE.map((date, index) =>
                                        <td key={index} className={`schedule-cell`}>
                                            {
                                                events.filter(item =>
                                                    item.event.date == date &&
                                                    item.event.time == time
                                                ).map((event, eventID) =>
                                                    <div key={eventID} className={`cell-content ${!event?.value && 'empty'}`}>
                                                        <span className={'cell-value'}>{event?.value}</span>
                                                        <span className={'cell-type'}>{event?.type}</span>
                                                    </div>
                                                )
                                            }
                                        </td>
                                    )
                                }
                            </tr>
                            )
                        }
                        </tbody>
                    </table>
                </section>
            </article>
        </ContentWrapper>
    )
}

const Schedule = memo(scheduleFC)

export default Schedule;