import React, {memo} from "react";

import {useMsal} from "@azure/msal-react";
import {Redirect} from "react-router";

const logoutFC:React.FC = () => {
    const { instance } = useMsal();

    instance.logout()

    return <Redirect to={'/app_login'} />
}

const Logout = memo(logoutFC)

export default Logout;