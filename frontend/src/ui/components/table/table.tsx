import React, {memo} from "react";
import {Link} from "react-router-dom";

import './table.scss'

export type Header = {
    value:string,
    fullValue?:string,
    id:string,
    label?:string
}

export type Mark = {
    id:string,
    mark: number
}

export type Student = {
    name:string,
    id:string,
    label?:string,
    marks: Mark[]
}

export type Group = {
    name:string,
    id:string,
    isMeta?:boolean,
    lecture?:{
        date:string,
        time:string
    },
    seminar?:{
        date:string,
        time:string
    },
    label?:string,
    students: Student[]
}

export enum DisplayMode {
    'all'
}

type Props = {
    topHeader: Header[],
    groups: Group[],
    displayMode: DisplayMode | string
    stats?:boolean
}

const tableFC:React.FC<Props> = ({topHeader,groups, displayMode}:Props) => {

    const getTableContent = () => {
        if (displayMode == DisplayMode.all) {

            return groups.map((group, index) => {
                return (
                    <>
                        <tr key={index} className={'group-cell'}>
                            <td colSpan={topHeader.length + 2}>{group.name}</td>
                        </tr>
                        {
                            group.students.map((student, studentIndex) =>
                                <tr key={studentIndex} className={'table-row'}>
                                    <th className={'table-row-header'}>{studentIndex + 1}</th>
                                    <th className={'table-row-header'}><Link to={'/app/profile/'+student.id}>{student.name}</Link></th>
                                    {
                                        (
                                            () => {
                                                const marks: any = student.marks.reduce && student.marks.reduce((acc, el) => ({
                                                    ...acc,
                                                    [el.id]: el.mark
                                                }), {})

                                                return topHeader.map((item, index) =>
                                                    <td className={`table-cell ${index == topHeader.length - 1 ? 'fixed' : ''}`}
                                                        key={index}>
                                                        {
                                                            !isNaN(marks && marks[item.id]) ?
                                                                <Link to={'/app/check/'+item.id}>
                                                                    {
                                                                        !isNaN(marks[item.id]) ?
                                                                            marks[item.id] :
                                                                            '-'
                                                                    }
                                                                </Link> :
                                                                <span> - </span>
                                                        }
                                                    </td>
                                                )
                                            })()
                                    }
                                </tr>
                            )
                        }
                    </>
                )
            })
        } else {
            const group = groups.find(group =>
                group.students.find(student =>
                    student.id == displayMode
                )
            )

            const student = group?.students.find(student =>
                student.id == displayMode
            )

            const marks: any = student?.marks?.reduce && student?.marks?.reduce((acc, el) => ({...acc, [el.id]: el.mark}), {})

            if (!(group && student))
                return (
                    <tr>
                        <td colSpan={topHeader.length + 2}>error</td>
                    </tr>
                )

            return (
                <>
                    <tr className={'group-cell'}>
                        <td colSpan={topHeader.length + 2}>{group.name}</td>
                    </tr>
                    <tr className={'table-row'}>
                        <th className={'table-row-header'}>1</th>
                        <th className={'table-row-header'}><Link to={'/app/profile/'+student.id}>{student?.name}</Link></th>
                        {
                            topHeader.map((item, index) =>
                                <td key={index}
                                    className={`table-cell ${index == topHeader.length - 1 ? 'fixed' : ''}`}>
                                    {
                                        !isNaN(marks && marks[item.id]) ?
                                            <Link to={'/app/tasks/'+item.id}>
                                                {
                                                    !isNaN(marks[item.id]) ?
                                                        marks[item.id] :
                                                        '-'
                                                }
                                            </Link> :
                                            <span> - </span>
                                    }
                                </td>
                            )
                        }
                    </tr>
                </>
            )
        }
    }

    return (
        <table className={'table'}>
            <thead className={'table__header'}>
            <tr>
                <th>№</th>
                <th>Студент</th>
                {
                    topHeader.map((item, index) =>
                        <th key={index} className={`cell ${index == topHeader.length - 1 ? 'fixed' : ''}`}>
                            <Link to={`/app/${displayMode == DisplayMode.all ? 'check' : 'tasks'}/`+item.id} title={item.fullValue}>{item.value}</Link>
                        </th>
                    )
                }
            </tr>
            </thead>
            <tbody className={'table__content'}>
            {
                getTableContent()
            }
            </tbody>
        </table>
    )
}

const Table = memo(tableFC)

export default Table;