import React, {memo, useEffect, useState} from "react";

import './notifications.scss'
import ContentWrapper from "../content-wrapper/content-wrapper";
import {EyeOpenIcon,  QuestionMarkIcon, UpdateIcon} from "@modulz/radix-icons";
import {TAGS} from "../news/news";
import Question from "../question/question";
import {useApiContext} from "../../utils/contexts/api-context";
import {useStore} from "react-redux";

export type NotificationPost = {
    author:string,
    authorID:string,
    postID:string,
    msg:string,
    date:string,
    tags:string[],
    isViewed:boolean,
    source?: string,
}

const notificationsFC:React.FC = () => {
    document.title = "ИО - Уведомления"

    const api = useApiContext()

    const fetchData = async () => {
        let test: NotificationPost[] = []

        await api?.get('notifications').then((res) => {
            test = res.map((item: any) => {
                return {
                    ...item,
                    tags: JSON.parse(item.tags) || [],
                    authorID: '001',
                    postID: item.id,
                    isViewed: item.is_viewed
                }
            }).reverse()
        })

        return test
    }

    const [viewMode, setViewMode] = useState(false)

    const [quest, setQuest] = useState<boolean>(false)

    const openQuest = () => {
        setQuest(true)
    }

    const closeQuest = () => {
        setQuest(false)
    }

    const [posts, setPosts] = useState<NotificationPost[]>([])
    const [allPosts, setAllPosts] = useState<NotificationPost[]>([])

    const changeViewMode = () => {
        setViewMode(!viewMode)
    }

    const updatePosts = () => {
        const currentPosts = allPosts.filter(item => !(!viewMode && item.isViewed))

        setPosts(currentPosts)
    }

    const setViewedAll = () => {
        const currentPosts = [...posts].map((item: any) => item.postID)

        const allPosts = [...posts]

        allPosts.filter((item: any) => !item.isViewed).forEach((item: any) => {
            setViewed(item.postID)
        })

        api?.put(`notifications/viewed`, currentPosts).then(() => {
            updatePosts()
        })
    }

    const setViewed = (id:string) => {
        const currentPosts = [...posts]
        const currentPost = currentPosts.find(item => item.postID === id)

        if(currentPost)
            currentPost.isViewed = true

        api?.put(`notifications/viewed`, [currentPost?.postID]).then(()=>{
            setPosts(currentPosts)
            updatePosts()
        })
    }

    const updateAllPosts = () => {
        fetchData().then((data) => {
            setAllPosts(data)
        }).then(() => {
            setViewMode(false)
            updatePosts()
        })
    }

    useEffect(() => {
        fetchData().then((data) => {
            setAllPosts(data)
        })
    }, [])

    useEffect(() => {
        updatePosts()
        //
    }, [allPosts, viewMode])

    return (
        <ContentWrapper minWidth={950} maxWidth={950}>
            <article className={'notifications'}>
                <header className={'notifications-header'}>
                    <div className={'notifications-header__info'}>
                        <h2 className={'notifications-header__h2'}>Уведомления</h2>
                    </div>
                    <div className={'notifications-header__buttons'}>
                        <button className={'button'} onClick={changeViewMode}>
                            {
                                viewMode ?
                                    <>Показать непрочитанные</> :
                                    <>Показать все</>
                            }
                        </button>
                        <button className={'button'} onClick={setViewedAll}>
                            Пометить все как прочитанные
                        </button>
                        <button className={'button'} onClick={openQuest}>
                            <span className={'icon'}><QuestionMarkIcon/></span>
                        </button>
                        <button onClick={updateAllPosts} className={'button'}><span
                            className={'icon'}><UpdateIcon/></span>
                        </button>
                    </div>
                </header>
                <section className={'notification-data'}>
                    {
                        posts.length == 0 &&
                            <h3 className={'empty'}>
                                Непрочитанных уведомлений нет!
                            </h3>
                    }
                    {
                        posts.map((item, index) =>
                            <div key={index}
                                 className={`notification-post _${TAGS.filter(tag => (item.tags[0] || '') === tag.msg)[0]?.color || ''}`}>
                                <div className={'notification-post__header'}>
                                    <span className={'notification-post__author'}>Источник: {item.author}</span>
                                    <div className={'notification-post__buttons'}>
                                        {
                                            item.isViewed ?
                                                <div className={'viewed'}>
                                                    Прочитано
                                                </div> :
                                                <div className={'button'} onClick={setViewed.bind(null,item.postID)}><span
                                                    className={'icon'}><EyeOpenIcon/></span>
                                                </div>
                                        }
                                    </div>
                                </div>
                                <pre className={'notification-post__message'}>
                                    {item.msg}
                                </pre>
                                {
                                    item.source &&
                                    <div className={'source'}>
                                        <span className={'source__text'}>
                                            Ссылка: <a href={item.source}>{item.source}</a>
                                        </span>
                                    </div>
                                }
                                <div className={'notification-post__info'}>
                                    <div className={'notification-post__tags'}>
                                        {item.tags.map((item, index) => {
                                            const color = TAGS.filter(tag => item === tag.msg)[0].color || 'grey'

                                            return <div key={index} className={`tag _${color}`}>{item}</div>
                                        })}
                                    </div>
                                    <div className={'notification-post__date'}>
                                        {item.date}
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </section>
            </article>
            <Question trigger={quest} message={'Вы можете настроить уведомления в профиле!'}
                      buttonMessage={'Продолжить'} action1={closeQuest} action2={closeQuest} justMessage={true}/>
        </ContentWrapper>
    )
}

const Notifications = memo(notificationsFC)

export default Notifications;
