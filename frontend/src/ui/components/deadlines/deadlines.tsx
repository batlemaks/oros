import React, {memo, useEffect, useState} from "react"

import './deadlines.scss'
import {UpdateIcon} from "@modulz/radix-icons";
import {Link} from "react-router-dom";
import ContentWrapper from "../content-wrapper/content-wrapper";
import {useApiContext} from "../../utils/contexts/api-context";

type Deadline = {
    name:string,
    date:string,
    time:string,
    task_id?:string,
}

const deadlinesFC : React.FC = () => {
    document.title = "ИО - Текущие дедлайны"

    const api = useApiContext()

    const dataFetch = async () => {
        const  data:{
            deadlines:Deadline[]
        } = {
            deadlines: []
        }

        await api?.get('deadlines').then((res) => {
            data.deadlines = res.filter((item: any) => Date.parse(item.time) > Date.now())
        })

        return data
    }

    const [deadlines,setDeadlines] = useState<Deadline[]>()

    const update = () => {
        dataFetch().then((data) => {
            setDeadlines(data.deadlines)
        })
    }

    useEffect(() => {
        update()
    },[])

    return (
        <ContentWrapper minWidth={400} maxWidth={900}>
            <article className={'deadlines'}>
                <header className={'deadlines__header'}>
                    <div className={'deadlines__info'}>
                        <h2 className={'deadlines__h2'}>
                            Текущие дедлайны
                        </h2>
                    </div>
                    <div className={'deadlines__buttons'}>
                        <button onClick={update} className={'button'}>
                            <span className={'icon'}>
                                <UpdateIcon/>
                            </span>
                        </button>
                    </div>
                </header>
                <section className={'deadlines__data'}>
                    <div className={'deadlines__list'}>
                        {
                            deadlines && deadlines.map((deadline, index) =>
                                <div key={index} className={'deadline'}>
                                    <div className={'deadline__info'}>
                                        <span className={'deadline__name'}>{deadline.name}</span>
                                        {
                                            deadline.task_id &&
                                                <Link className={'deadline__link'} to={'tasks/'+deadline.task_id}>Задание</Link>
                                        }
                                    </div>
                                    <span className={'deadline__date'}>{deadline.time}</span>
                                </div>
                            )
                        }
                    </div>
                </section>
            </article>
        </ContentWrapper>
    )
}

const Deadlines = memo(deadlinesFC)

export default Deadlines;
