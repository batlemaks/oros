import React, {memo, useMemo, useState} from "react";
import {BrowserRouter as Router, Redirect, Route, Switch, useRouteMatch} from 'react-router-dom';
import {MAIN_ROUTES, PUBLIC_PAGE_ROUTES} from "./utils/constants/routes-constatns";
import store from "./store/store";
import {Api} from "./utils/api/api";
import {DataProvider} from "./utils/contexts/data-context";
import PublicPage from "./routes/home-page/public-page";
import RouteRenderer from "./utils/route-renderer";
import {setUserData} from "./store/actions/user.actions";
import {UserRole} from "./utils/constants/permission-constants";
import {loginRequest, msalConfig} from "../../auth/authConfig";
import { PublicClientApplication } from "@azure/msal-browser";
import {AuthenticatedTemplate, UnauthenticatedTemplate, useMsal} from "@azure/msal-react";
import {callMsGraph} from "../../auth/graph";

enum Lang {
    RU,
    ENG
}

/**
 * Функциональная компонента
 * Отвечает за рендеринг Приложения
 * @return Приложение
 */
const appFC:React.FC = () => {
    const [lang, setLang] = useState(Lang.RU)

    /**
     * Экземпляр Api
     */
    const api: Api = useMemo(() => new Api(store), [])

    const dataProps = {
        api,
        lang,
        setLang,
        store
    }

    return (
        <DataProvider {...dataProps}>
            <Router>
                <AuthenticatedTemplate>
                    <RouteRenderer routes={MAIN_ROUTES} redirect={'/'}/>
                </AuthenticatedTemplate>
                <UnauthenticatedTemplate>
                    <RouteRenderer routes={PUBLIC_PAGE_ROUTES}/>
                </UnauthenticatedTemplate>
            </Router>
        </DataProvider>
    )
}

/**
 * Мемоизированная компонента App
 * Отвечает за приложение
 */
const App = memo(appFC)

export default App