import {combineReducers, Reducer} from "redux";
import {Actions} from "../actions/action";
import {user} from "./user.reducer";
import {User} from "../../utils/types/user-types";

export type State = {
    user: User
}

export const reducer: Reducer<State, Actions> = combineReducers<State, Actions>({
    user
})
