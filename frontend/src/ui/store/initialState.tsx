import {State} from "./reducers/reducer";

/**
 * Начальное значение хранилища
 */
const initialState:State = {
    user: {
        isAuthorized:false,
        userData: { role: 1}
    },
};

export default initialState;