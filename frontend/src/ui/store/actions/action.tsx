import {setUserData} from "./user.actions";

export type Actions =
     ReturnType<typeof setUserData>