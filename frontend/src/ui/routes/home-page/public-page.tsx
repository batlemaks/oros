import React, {memo} from "react";
import {Redirect, useLocation, useRouteMatch} from "react-router-dom";
import Login from "../../components/login/login";
import RouteRenderer from "../../utils/route-renderer";
import {RouteType} from "../../utils/types/route-types";
import Tabs from "../../components/tabs/tabs";
import {Tab, TabMode} from "../../components/tabs/tab";
import {useLangContext} from "../../utils/contexts/lang-context";
import InfoProgram from "../../components/info/infoProgram";
import InfoCourse from "../../components/info/infoCourse";

import './public.scss'

type TabItem = RouteType & {
    mode:TabMode,
    currentTab?: boolean,
    download?: boolean,
    href?: string
}

const RedirectToPublic : React.FC = () => <Redirect to={'/app_login/login'} />

export const LOGIN_ROUTES: TabItem[] = [{
        title:'ВХОД',
        path:'login',
        component:Login,
        mode:TabMode.tab,
        currentTab: true,
    },{
        title:'О Курсе',
        path:'about-course',
        component:InfoCourse,
        mode:TabMode.tab,
    },{
        title:'О Программе',
        path:'about-program',
        component:InfoProgram,
        mode:TabMode.tab,
    },{
        title:'Пользовательское соглашение',
        path:'qweqweqweweqwwqe',
        href:'qweqweqweweqwwqe',
        download: true,
        mode:TabMode.link,
    },{
        title:'Нашли ошибку?',
        path:'https://docs.google.com/forms/d/e/1FAIpQLSdyz3tWktdHjVybK4091EcNsEHgH3Ys4dtE8LxekmvjY45xPA/viewform',
        href:'https://docs.google.com/forms/d/e/1FAIpQLSdyz3tWktdHjVybK4091EcNsEHgH3Ys4dtE8LxekmvjY45xPA/viewform',
        mode:TabMode.link,
    },
    {
        title:'ВХОД',
        path:'',
        component:RedirectToPublic,
        mode:TabMode.none,
    },
]

export enum ZoneAlign {
    center,
    left,
    right,
}

export enum ColorTheme {
    light,
    dark,
    invisible
}

export type ZoneSettings = {
    align?: ZoneAlign,
    theme?: ColorTheme,
    countOfItems?: number,
}

export const PUBLIC_PAGE_ZONES:ZoneSettings[] = [{
    theme: ColorTheme.light,
    countOfItems: 4,
},{
    theme : ColorTheme.light,
}]

const publicPageFC:React.FC = () => {
    let {path, url} = useRouteMatch();

    path = path.replace(/\/$/, '');
    url = url.replace(/\/$/, '');
    const location = useLocation().pathname;
    const dataLang = useLangContext()

    return (
        <div className={'public-page'}>
            <div className={'public-page__info'}>
                <RouteRenderer path={url + '/'} routes={LOGIN_ROUTES}/>
                <Tabs zoneSettings={PUBLIC_PAGE_ZONES}
                      currentTabIndex={LOGIN_ROUTES.map(item => (url + '/' + item.path)).indexOf(location === '/app_login' ? '/app_login/login' : location) + 1}>
                    <Tab key={0}
                         message={'RU/RU'}
                         mode={TabMode.button}
                         onClick={dataLang?.setLang.bind(null, dataLang?.lang)}
                    />
                    {
                        LOGIN_ROUTES.map((item, index) =>
                            <Tab key={index + 1}
                                 message={item.title}
                                 mode={item.mode}
                                 link={`${path}/${item.path}`}
                                 href={item.path}
                                 download={item.download}
                                 currentTab={item.currentTab}
                            />)
                    }
                    <Tab key={LOGIN_ROUTES.length + 1}
                         message={new Date().getFullYear().toString()}
                    />
                </Tabs>
            </div>
        </div>
    )
}

const PublicPage = memo(publicPageFC)

export default PublicPage;