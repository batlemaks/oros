# OROS

OROS - Operations Research - Online System

Онлайн сервис для проведения автоматической проверки заданий по курсу Исследование Операций (ВШЭ)

## Для запуска

`npm run start` - Запуск дев-сервера

## Для разработки

`npm run dev` - Сборка дев-билда, в папке build (`карты кода`, `быстрая`, `без оптимизаций`)

`npm run build` - Сборка прод-билда, в папке build (`хеши`, `долгая`, `оптимизации`)

`npm run docs` - Сборка документации, в папке docs

## Технологии и утилиты

`npm i` - Установка всех пакетов

| Название:     | Установлена:    | Статус:    |
| :------------ | :-------------: | :--------: |
| React.js      | Да              | Работает   |
| Webpack       | Да              | Работает   |
| TypeDoc       | Да              | Работает   |
| TypeScript    | Да              | Работает   |
| SASS          | Да              | Работает   |
| PostCSS       | Да              | Работает   |
| ESLint        | Да              | Работает   |
| Redux         | Да              | Работает   |
| JUnit         | Нет             | В планах   |

## О разработке: 

### Стилизация:

`Normilize` -> `Vars` -> `Custom-reset` -> `{components style}`

Наименования классов - `БЭМ`

Препроцессор - `SCSS`

PostCSS:
1. `AutoPrefixer`
2. `CSSMQpacker`
3. `CSSNano`
4. ...

### ESLint - проверка кода

[Конфигуратор конфига](https://eslint.org/demo#eyJ0ZXh0IjoidmFyIGZvbyA9IGJhcjsiLCJvcHRpb25zIjp7InBhcnNlck9wdGlvbnMiOnsiZWNtYVZlcnNpb24iOjEyLCJzb3VyY2VUeXBlIjoic2NyaXB0IiwiZWNtYUZlYXR1cmVzIjp7ImpzeCI6dHJ1ZX19LCJydWxlcyI6eyJjb25zdHJ1Y3Rvci1zdXBlciI6MiwiZm9yLWRpcmVjdGlvbiI6MiwiZ2V0dGVyLXJldHVybiI6Miwibm8tYXN5bmMtcHJvbWlzZS1leGVjdXRvciI6Miwibm8tY2FzZS1kZWNsYXJhdGlvbnMiOjIsIm5vLWNsYXNzLWFzc2lnbiI6Miwibm8tY29tcGFyZS1uZWctemVybyI6Miwibm8tY29uZC1hc3NpZ24iOjIsIm5vLWNvbnN0LWFzc2lnbiI6Miwibm8tY29uc3RhbnQtY29uZGl0aW9uIjoyLCJuby1jb250cm9sLXJlZ2V4IjoyLCJuby1kZWJ1Z2dlciI6Miwibm8tZGVsZXRlLXZhciI6Miwibm8tZHVwZS1hcmdzIjoyLCJuby1kdXBlLWNsYXNzLW1lbWJlcnMiOjIsIm5vLWR1cGUtZWxzZS1pZiI6Miwibm8tZHVwZS1rZXlzIjoyLCJuby1kdXBsaWNhdGUtY2FzZSI6Miwibm8tZW1wdHkiOjIsIm5vLWVtcHR5LWNoYXJhY3Rlci1jbGFzcyI6Miwibm8tZW1wdHktcGF0dGVybiI6Miwibm8tZXgtYXNzaWduIjoyLCJuby1leHRyYS1ib29sZWFuLWNhc3QiOjIsIm5vLWV4dHJhLXNlbWkiOjIsIm5vLWZhbGx0aHJvdWdoIjoyLCJuby1mdW5jLWFzc2lnbiI6Miwibm8tZ2xvYmFsLWFzc2lnbiI6Miwibm8taW1wb3J0LWFzc2lnbiI6Miwibm8taW5uZXItZGVjbGFyYXRpb25zIjoyLCJuby1pbnZhbGlkLXJlZ2V4cCI6Miwibm8taXJyZWd1bGFyLXdoaXRlc3BhY2UiOjIsIm5vLW1pc2xlYWRpbmctY2hhcmFjdGVyLWNsYXNzIjoyLCJuby1taXhlZC1zcGFjZXMtYW5kLXRhYnMiOjIsIm5vLW5ldy1zeW1ib2wiOjIsIm5vLW9iai1jYWxscyI6Miwibm8tb2N0YWwiOjIsIm5vLXByb3RvdHlwZS1idWlsdGlucyI6Miwibm8tcmVkZWNsYXJlIjoyLCJuby1yZWdleC1zcGFjZXMiOjIsIm5vLXNlbGYtYXNzaWduIjoyLCJuby1zZXR0ZXItcmV0dXJuIjoyLCJuby1zaGFkb3ctcmVzdHJpY3RlZC1uYW1lcyI6Miwibm8tc3BhcnNlLWFycmF5cyI6Miwibm8tdGhpcy1iZWZvcmUtc3VwZXIiOjIsIm5vLXVuZGVmIjoyLCJuby11bmV4cGVjdGVkLW11bHRpbGluZSI6Miwibm8tdW5yZWFjaGFibGUiOjIsIm5vLXVuc2FmZS1maW5hbGx5IjoyLCJuby11bnNhZmUtbmVnYXRpb24iOjIsIm5vLXVudXNlZC1sYWJlbHMiOjIsIm5vLXVudXNlZC12YXJzIjoyLCJuby11c2VsZXNzLWNhdGNoIjoyLCJuby11c2VsZXNzLWVzY2FwZSI6Miwibm8td2l0aCI6MiwicmVxdWlyZS15aWVsZCI6MiwidXNlLWlzbmFuIjoyLCJ2YWxpZC10eXBlb2YiOjIsIm5vLXZhciI6Mn0sImVudiI6eyJub2RlIjp0cnVlLCJicm93c2VyIjp0cnVlLCJlczYiOnRydWUsImVzMjAxNyI6dHJ1ZSwiZXMyMDIwIjp0cnVlLCJlczIwMjEiOnRydWUsInNlcnZpY2V3b3JrZXIiOnRydWV9fX0=)

1. `eslint:recommended`
2. `plugin:react/recommended`

### PWA `В будущем`

1. Манифест
2. Иконки
3. Воркер

Возможно будет реализовано в будущем.
